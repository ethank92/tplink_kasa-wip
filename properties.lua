--[[ Properties File Contents ]]
table.insert(props,{
  Name = "Smart Plugs",
  Type = "integer",
  Min = 0, Max = 32, Value = 1
})

table.insert(props,{
  Name = "Smart Power Strips (KP303)",
  Type = "integer",
  Min = 0, Max = 12, Value = 0
})

table.insert(props,{
  Name = "Smart Bulbs",
  Type = "integer",
  Min = 0, Max = 32, Value = 0
})