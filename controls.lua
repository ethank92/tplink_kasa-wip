
---
ctl_type = "Indicator"
---
table.insert(ctrls,{
  Name = "Ready",   
  ControlType = ctl_type, IndicatorType = "Led",
  PinStyle = "Output", UserPin = false, 
  Count = 1
})

table.insert(ctrls,{
  Name = "Status LED",  
  ControlType = ctl_type, IndicatorType = "Led",
  PinStyle = "Output", UserPin = true, 
  Count = 1  
})

table.insert(ctrls,{
  Name = "Status",      
  ControlType = ctl_type, IndicatorType = "Status",
  PinStyle = "Output", UserPin = true, 
  Count = 1
})

table.insert(ctrls,{
  Name = "Smart Power Strip",
  ControlType = ctl_type, IndicatorType = "Text",
  PinStyle = "Output", UserPin = true, 
  Count = 12
})

---
ctl_type = "Text"
---
table.insert(ctrls,{
  Name = "Username", 
  ControlType = ctl_type,
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls,{
  Name = "Password", 
  ControlType = ctl_type,  
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls,{
  Name = "Secured",
  ControlType = ctl_type,
  PinStyle = "Output", UserPin = true, 
  Count = 1
})

table.insert(ctrls,{
  Name = "Server", 
  ControlType = ctl_type, TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls,{
  Name = "Smart Bulb Color", 
  ControlType = ctl_type,
  PinStyle = "Both", UserPin = true, 
  Count = 32
})

table.insert(ctrls,{
  Name = "Smart Bulb Kelvin K", 
  ControlType = ctl_type, TextBoxType = "Indicator",
  PinStyle = "Both", UserPin = true, 
  Count = 32
})

---
ctl_type = "Knob"
---
table.insert(ctrls,{
  Name = "Smart Bulb Hue", 
  ControlType = ctl_type, 
  ControlUnit = "Integer",
  Min = 0, Max = 360,  
  PinStyle = "Both", UserPin = true, 
  Count = 32
})

table.insert(ctrls,{
  Name = "Smart Bulb Saturation", 
  ControlType = ctl_type,
  ControlUnit = "Integer",
  Min = 0, Max = 100,  
  PinStyle = "Both", UserPin = true, 
  Count = 32
})

table.insert(ctrls,{
  Name = "Smart Bulb Value", 
  ControlType = ctl_type,
  ControlUnit = "Integer",
  Min = 0, Max = 100,  
  PinStyle = "Both", UserPin = true, 
  Count = 32
})

table.insert(ctrls,{
  Name = "Smart Bulb Kelvin", 
  ControlType = ctl_type,
  ControlUnit = "Integer",
  Min = 0, Max = 100,  
  PinStyle = "Both", UserPin = true, 
  Count = 32
})

---
ctl_type = "Button"
---
table.insert(ctrls,{
  Name = "Connect",
  ControlType = ctl_type, ButtonType = "Toggle",
  PinStyle = "Input", UserPin = true,
  Count = 1
})

table.insert(ctrls,{
  Name = "Smart Plug", 
  ControlType = ctl_type, ButtonType = "Toggle", 
  PinStyle = "Both", UserPin = true, 
  Count = 32 
})

table.insert(ctrls,{
  Name = "Smart Plug RSSI", 
  ControlType = ctl_type, ButtonType = "Toggle",  
  PinStyle = "None", UserPin = true, 
  Count = 32
})

table.insert(ctrls,{
  Name = "Smart Power Strip Plug",
  ControlType = ctl_type, ButtonType = "Toggle",  
  PinStyle = "Both", UserPin = true, 
  Count = 12*3
})

table.insert(ctrls,{
  Name = "Smart Power Strip RSSI", 
  ControlType = ctl_type, ButtonType = "Toggle",
  UserPin = true, PinStyle = "None", 
  Count = 32
})

table.insert(ctrls,{
  Name = "Smart Bulb",
  ControlType = ctl_type, ButtonType = "Toggle",
  UserPin = true, PinStyle = "Both",
  Count = 32
})

table.insert(ctrls,{
  Name = "Smart Bulb RSSI",
  ControlType = ctl_type, ButtonType = "Toggle", 
  UserPin = true,PinStyle = "None",
  Count = 32
})

