# TP Link Kasa

This is a Q-SYS Plugin for Control of TP Link Kasa via the [Cloud API](https://www.npmjs.com/package/tplink-cloud-api).
This plugin is NOT finished. Please feel free to improve!
There is a local IP protocol that I want to implement later.

> Bug reports and feature requests should be sent to Ethan Klein (ethan.klein@qsc.com)

## KNOWN BUGS:
At least one Smart Power Strip is required to draw Smart Bulb devices in UI

## How do I get set up?

See [Q-SYS Online Help File - Plugins](https://q-syshelp.qsc.com/#Schematic_Library/plugins.htm)

### Configuration
![Properties](./screenshots/tplink_props.png)

#### Plugin supports limited models:
#### Smart Plugs: (HS103, HS105) 
Min: 0 
Max: 32
#### Smart Power Strips: (KP303)
Min: 0 
Max: 12
#### Smart Bulbs: (KL130)
Min: 0
Max: 32

![Settings Page](./screenshots/tpplink_settings.png)
#### Status
Shows current connection status of plugin to Cloud API

#### Server
There are two servers to choose from. Choose your server based on your device / account region lock. 

#### Username
Account name for Cloud API - same username used to sign into TP Link Kasa phone app.

#### Password
Password for Cloud API - same password used to sign into TP Link Kasa phone app.

#### Connect
Toggle button to toggle connection to Cloud API. Connection and credentials will persist on redpploy.

### Smart Plugs (HS103, HS105)

![Smart Plugs Page](./screenshots/tplink_hs103_hs105.png)
#### Plug Name
User configurable name for the outlet - read only

#### Power Toggle Button
Toggle button for outlet state

- Cyan = On
- Grey = Off
- Offline or NA = Control Disabled / Default Name = Plug

#### Signal Strength (RSSI)
Indicates the current wireless signal strength to the device

### Smart Power Strips (KP303)
![Smart Power Strips Page](./screenshots/tplink_kp303.png)
#### Power Strip Name
User configurable name for the power strip - read only

#### Plug Name
User configurable name for the outlet on a specific power strip - read only

#### Power Toggle Button
Toggle button for outlet state on a specific power strip

- Cyan = On
- Grey = Off
- Offline or NA = Control Disabled / Default Name = Plug

#### Signal Strength (RSSI)
Indicates the current wireless signal strength to the device

### Smart Bulbs (KL103)
![Smart Bulbs Page](./screenshots/tplink_kl130.png)
#### Bulb Name
User configurable name for bulb - read only

#### Power Toggle Button
Toggle button for the bulb state. This button's color will also reflect the current color settings.

- Color: On (Current Color)
- Grey: Off
- Offline or NA = Control Disabled / Default Name = Bulb

#### Color Control
Color control for two modes: RGB or Kelvin
- Setting Kelvin to 0 will automatically change Bulb to RGB Mode
- Setting Kelvin to non 0 will automatically change Bulb to Kelvin Mode

- Color: Hex color code for current setting (RGB Only) 
- Hue: Hue for current setting (RGB Only)
- Sat: Saturation for current setting (RGB Only)
- Val: Brightness for current setting (RGB or Kelvin)
- Kelvin: Color temperature for current setting (Kelvin Mode Only)

#### Signal Strength (RSSI)
Indicates the current wireless signal strength to the device