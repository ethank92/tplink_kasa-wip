--[[Remove '=' to enable runtime]]
rapidjson = require("rapidjson")

Update = Timer.New()
RenewAuth = Timer.New()

Controls["Server"].Choices = {"https://wap.tplinkcloud.com", "https://eu-wap.tplinkcloud.com"}
StatusLEDColors = {"lime","orange","red","gray","red","blue"}

rssi_bars = {
  --rapidjson.encode({DrawChrome = true, IconData = ""}),
  rapidjson.encode({DrawChrome = true, IconData = "iVBORw0KGgoAAAANSUhEUgAAAZMAAAG4CAYAAACAdwqUAAABhWlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TS0UqChYRcchQnSz4hThKFYtgobQVWnUwufRDaNKQpLg4Cq4FBz8Wqw4uzro6uAqC4AeIk6OToouU+L+k0CLGg+N+vLv3uHsHCPUyU82OMUDVLCMVj4nZ3IoYfEUI/ejFOAISM/VEeiEDz/F1Dx9f76I8y/vcn6NbyZsM8InEs0w3LOJ14ulNS+e8TxxmJUkhPiceNeiCxI9cl11+41x0WOCZYSOTmiMOE4vFNpbbmJUMlXiKOKKoGuULWZcVzluc1XKVNe/JXxjKa8tprtMcQhyLSCAJETKq2EAZFqK0aqSYSNF+zMM/6PiT5JLJtQFGjnlUoEJy/OB/8LtbszA54SaFYkDgxbY/hoHgLtCo2fb3sW03TgD/M3CltfyVOjDzSXqtpUWOgJ5t4OK6pcl7wOUOMPCkS4bkSH6aQqEAvJ/RN+WAvluga9XtrbmP0wcgQ10t3QAHh8BIkbLXPN7d2d7bv2ea/f0Ajn5yshGqn1YAAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAADdcAAA3XAUIom3gAAAAHdElNRQfkDBYSFwDrouX+AAAJBElEQVR42u3bO48dZx3A4d/evF7Ltnw3wcRCBCKZkEh8AKCDNFR0iA8BBXwIS1QpqBGJBAUNAgnR0oDokAiKIgIxJNgksXHsxFe8FDsBO949c2JH0bk8T7Mr2cXs/52Z38x7dlcC4FFtVgerjWp1gX/Oe9Xt6lp1Z7f/sOJcAPhIVqqT1dkhJMtku7pavV5dEROAR7NenauOG0WXqleGt5bWzANgKmvVl6sjRlHDW9nB6i0xAZjeueqoMTzgwPD136tmATDqUHXKGHZ1ttoUE4BxZ4xgT6vVaTEBGGd7a7JjYgIw2Vo7f0/C3rbEBGCyDSMYn5GYAEzm7/HGrYoJAI9fEyMAQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAEBMAxAQAMQFgAawbAcyUjQW/Lu9Vd4aviAnwMVmtnqhOVodbnt2CG9Xl6o3qfaeBmACP7kh1rtpcwp99qzpTfbr6R/Vate2UmO+nIuCTd7J6bklDcr+V6snq2eF7xASY0qHhjcT193/Hqs8bg5gA0/uCa29XZ9r53AgxAUYcccOc6EkjEBNg3DEjmOhoPjsRE2DUISOYaL2d3/RCTIAJNoxg1D4jEBNgsjUjcF+yaAAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAICYAiAkAYgKAmACAmAAgJgCICQALYd0IFs5KdbDaWoL1vVvdqK5X25YexITHt686W31qCdf1bnWxulDddiqAmPBojlVfXOL1XK8+Uz1RvVy945SAT5bPTObf8epZDwZVrVVfGmYCiAlT2hreSFaM4n9WqnPVfqMAMWE6nx2exnnQ+jAbQEyY4oZ5yhj2dDpbfyAmjDqa7a1JVoYZAWLCBFtGYEYgJjyuDSMwIxATHpctLuc3uNgAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAKa0PvLvW9XnqhPVxgLP4Xr1ZnXBKQHw8cXkG9X3qq8OQVkWF6qfV+eri04PgOl8eJvrcPWL6tdDULaWbB5nh4j+pfqO0wPgo7+ZHK5+Wz1nLB2oflKdrn5oHADTv5m8KCQPOV993RgApovJ89U3jWPX+bzQ+C8qAIhJ9V2j2NPT3k4AxmNyoPqaUUz0vBEATI7JU9V+o5joGSMAmByTE8Yw6pQRAEyOibeScWYEMBITABATAMQEADEBQEwAQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAxAQAxAQAMQFATAAQEwAQEwDEBAAxAUBMAEBMABATAMQEADEBADEBQEwAEBMAxAQAxAQAMQFgvmJy2xhGzeKMti3LqHuOaS5tO575u9ZWq7fNYdS/ZvCY7liWuZyRdZu/hzdrNsWMVqu/VnfNYqJXZ/CYbliWuZyRdRt/C7g5Y8f0n+qWpZl8Xq9W71a/M4uJfjODx3TF6/foTenKjK4be7s63Lyt23y5/MEH8D8yiz29Wf1qBo/rbrO5/TYrLs3oG/fbnnJHr7dZ9Ial2dO96tIHMflp9Xsz2dUPmt2tCVuUe4f2bzN84b1miXb1bvXWjB7bNQ9ve7pQ3Vq97wT/lvo+5IXqpRk+vpvVn7Pddb/tYSY3Z/gYL7nWHnKr+tOMn8uvVNct1QPeqV6vWvtQeX9WfaU644bU+er7c3CjvjE80Z3I3w3drV4eTvBZd3l4iDtSrSz5ul2v/tjsb/9tDw8CB6sDOtKl+x9m13Z5lftxO/uWT1fHl/Dp6JfVt6sX5+iJ/2Z1cbgpbe2yrovuTvXPISTz9OR4dYjKvmr/EkblRjtbta82P79+u93OVtz7w5ptLuGD9tXhLe3v998jx07ep6pnhqfe9QUe0HvDtsMfhu/n2crw5LS/2liCiNwcAjLvW31r1eEhLIv8MLA9rNt7LcavSW8O19vGgu8M3Gvn73+u7RX+/wIwqsfEcnogOQAAAABJRU5ErkJggg=="}),
  rapidjson.encode({DrawChrome = true, IconData = "iVBORw0KGgoAAAANSUhEUgAAAZMAAAG4CAYAAACAdwqUAAABhWlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TS0UqChYRcchQnSz4hThKFYtgobQVWnUwufRDaNKQpLg4Cq4FBz8Wqw4uzro6uAqC4AeIk6OToouU+L+k0CLGg+N+vLv3uHsHCPUyU82OMUDVLCMVj4nZ3IoYfEUI/ejFOAISM/VEeiEDz/F1Dx9f76I8y/vcn6NbyZsM8InEs0w3LOJ14ulNS+e8TxxmJUkhPiceNeiCxI9cl11+41x0WOCZYSOTmiMOE4vFNpbbmJUMlXiKOKKoGuULWZcVzluc1XKVNe/JXxjKa8tprtMcQhyLSCAJETKq2EAZFqK0aqSYSNF+zMM/6PiT5JLJtQFGjnlUoEJy/OB/8LtbszA54SaFYkDgxbY/hoHgLtCo2fb3sW03TgD/M3CltfyVOjDzSXqtpUWOgJ5t4OK6pcl7wOUOMPCkS4bkSH6aQqEAvJ/RN+WAvluga9XtrbmP0wcgQ10t3QAHh8BIkbLXPN7d2d7bv2ea/f0Ajn5yshGqn1YAAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAADdcAAA3XAUIom3gAAAAHdElNRQfkDBYSFiIn2ZVbAAAJLUlEQVR42u3czYtddx3A4U/mpWlCEvPaqqGBNjYQioW6caegUIsQEASRqv9CqRvBld2JBVddiCuRpuhGBLEigm7cKAW7EqnB1EZTGtsmTVObpmkSF3NSk2bmnkkayp17n2czA5PFud/fOfdzzrnnZlMA3KrN1bZquVqY4dd5uXq3OlddXO0fbLIvANyUTdW+6sAQknlypTpbvVSdEROAW7NUHa72GEWnqheGq5YWzQNgXRarh6qdRlHDVdm26lUxAVi/w9UuY7jO1uHnGwtmATBqe3WXMazqQLVZTADG7TeCNS1Ud4sJwDi3tybbLSYAky228n0S1rZFTAAmWzaC8RmJCcBkvo83bkFMAPjwNTECAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBADEBQEwAEBMAZsCSEcBUWZ7x4/JydXH4iZgAt8lC9YlqX7Wj+blbcL46XZ2s3rYbiAlw63ZWh6vNc/jat1T7q09W/66OV1fsEhv7rAj46O2rHpzTkFxrU3VP9enhd8QEWKftwxWJ4+//dlefMgYxAdbvfsfeqva38rkRYgKM2OkNc6J7jEBMgHG7jWCiXfnsREyAUduNYKKlVp70QkyACZaNYNQdRiAmwGSLRuB9yaIBgJgAICYAiAkAYgIAYgKAmAAgJgCICQCICQBiAoCYACAmACAmAIgJAGICgJgAgJgAICYAiAkAYgIAYgKAmAAgJgCICQCICQBiAoCYACAmACAmAIgJAGICgJgAgJgAICYAiAkAYgIAYgKAmAAgJgCICQCICQBiAoCYACAmACAmAIgJAGICgJgAgJgAICYAiAkAYgIAYgKAmAAgJgCICQCICQBiAoCYACAmACAmAIgJAGICgJgAgJgAICYAiAkAYgIAYgKAmAAgJgCICQCICQBiAoCYACAmACAmAIgJAGICgJgAgJgAICYAiAkAYgIAYgKAmAAgJgCICQCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAzIQlI5g5i9Vnqvuqj834az1bHa/+Ul2y9CAmfHgfr75bfavaNWev/Uz1dPX96hW7Anz03OaaDQ9Xf6sem8OQNLzmx6q/V0fsDiAm3Lwj1W+qnUbR9uqXggJiws05WD3TyuckrFisjlb3GgWICevzxHA2zvV2DLMBxIQRu6qvG8OaHs2tPxATRn0xT+NNslR9wRhATJjsfiMYdcgIQEyYbI8RjNpnBCAmTLZsBGYEYgKAmACAmAAgJgCICQBiAgBiAoCYACAmAIgJAIgJAGICgJgAICYAICYAiAkAYgKAmACAmAAgJgCICQBiAgBiAoCYACAmAIgJAIgJAGICgJgAICYAICYAiAkAYgKAmACAmAAgJgCICQBiAgBiAoCYACAmAIgJAIgJAGICgJgAICYAICYAiAkAYgKAmACAmAAgJgCICQBiAgBiAoCYACAmAIgJAIgJAGICgJgAICYAsD5LI3/fUt1X7a2WZ3gOb1UvVyfsEgC3LyZfqr5dfW4Iyrw4Uf2ierJ6xe4BsD4fvM21o/pV9dshKFvmbB4Hhoj+o/qm3QPg5q9MdlR/rB40lrZWT1d3Vz80DoD1X5kcFZIbPFk9bAwA64vJI9UR41h1Pk81/qACgJhUjxvFmg65OgEYj8nW6vNGMdEjRgAwOSYHqzuNYqIHjABgckz2GsOou4wAYHJMXJWMMyOAkZgAgJgAICYAiAkAYgIAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQAbKybvGsOoaZzRRcuyIWd02bKMumJ7NpzLC9Vr5jDqP1O4TdZtY66bk4CNd/JmzdYxo4Xqxeo9s5jomG2ybrfJecsyehXwzpRt06XqgqWZvF8vVG9WfzKLiX43hdv0eycBE71X/WEKt+uMpZno7PDmbd02ltNXP4D/kVms6eXq2Sncrjeqn1meNT0zzGjavOYsd/R4m0YnLc2aLlenrsbk59WfzWRV32l6b018b7iy5Maz2yem+MA7bolW9Wb16pRu27mm8zO4aXCiurBwzQ7+VfW9wVPDGe60erF6NLe7rnWp+kb1zynexlOOtRtcqP7adD859UL1lqW6zuvVS3X990xOVp+tnjOfrlQ/qB7fANv6bPXl3NO9emb7labztuQHHRuuUDx2uvIG/XzTf/vv0rCdr1uy90+K3j8BWFzlUu6nrdy3PFTtmcOzo18PZ/tHN9CBfrz6SbU8rNuWOVu309WPq68NB/tGcXbY9juqO6tNc7Zu54er62NtnMdvr7RyK+7tYc02z+GJ9tnhKu1f175Hju28B6sHqr3V0gwP6L/Dldlzw+8b2WL1UHVvtWvGd+wzw5vR803nE0A3u247hrAszvib0cXhOJuFx6Q3V9uGE7lZ/h9FLrfy/Z9za4X/f9BcyKKDf7KXAAAAAElFTkSuQmCC"}), 
  rapidjson.encode({DrawChrome = true, IconData = "iVBORw0KGgoAAAANSUhEUgAAAZMAAAG4CAYAAACAdwqUAAABhWlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TS0UqChYRcchQnSz4hThKFYtgobQVWnUwufRDaNKQpLg4Cq4FBz8Wqw4uzro6uAqC4AeIk6OToouU+L+k0CLGg+N+vLv3uHsHCPUyU82OMUDVLCMVj4nZ3IoYfEUI/ejFOAISM/VEeiEDz/F1Dx9f76I8y/vcn6NbyZsM8InEs0w3LOJ14ulNS+e8TxxmJUkhPiceNeiCxI9cl11+41x0WOCZYSOTmiMOE4vFNpbbmJUMlXiKOKKoGuULWZcVzluc1XKVNe/JXxjKa8tprtMcQhyLSCAJETKq2EAZFqK0aqSYSNF+zMM/6PiT5JLJtQFGjnlUoEJy/OB/8LtbszA54SaFYkDgxbY/hoHgLtCo2fb3sW03TgD/M3CltfyVOjDzSXqtpUWOgJ5t4OK6pcl7wOUOMPCkS4bkSH6aQqEAvJ/RN+WAvluga9XtrbmP0wcgQ10t3QAHh8BIkbLXPN7d2d7bv2ea/f0Ajn5yshGqn1YAAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAADdcAAA3XAUIom3gAAAAHdElNRQfkDBYSFQ4+LKp7AAAJY0lEQVR42u3cX6jedR3A8ffO2Wl/3JZrc1aWoMOBCIEGSUQLjFSoXVkRVldd1YVU9IeIyLtg0E2DoquITSoiglEyirqJqAiSLkLU/F/i0pxzlk73p4vzW2zznOc5U5nPeZ7X62YHtovf7/P9nd/79+d5tiYAXq111aZqoZqb4v08Vb1UHateXuofrHEsAFyQNdVl1ZVDSGbJ6epo9Wh1REwAXp211bXVNqPocHXfcNfSvHkArMh8dX11qVHUcFe2qXpKTABW7tpqqzGcY+Pw57NzZgEw1uZqhzEs6cpqnZgAjHeFESxrrrpcTADG83hrtLeICcBo8y1+n4TlbRATgNEWjGD8jMQEYDTfxxtvTkwAeO01MQIAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQEAMQFATAAQEwCmwFojgImxptpebZ7ifXyperp60XKLCfD6WV99prqtel/1phnZ779Xh6p91f0Og+m4EgLeGLurA9U7Z3gGJ6rvVF8dfp5EG6obHa6jeWcCb4yPVb+e8ZDU4tORL1YHqwWHxeo1bwRw0b17OHmuN4r/u6baVt09gdu2UL3DErkzgUmzr8VHJ5zrs9V7jEFMgPF2V+81hiWtqb5kDGICjPdhIxjplrw7ERNgrBuMYKQt1U5jEBNgtB1GYEZiArxWXryPd4kRiAkAYgIAYgKAmAAgJgCICQCICQBiAoCYACAmACAmAIgJAGICgJgAgJgAICYAiAkAYgIAYgKAmAAgJgCICQCICQBiAoCYACAmACAmAIgJAGICgJgYAQBiAoCYACAmACAmAIgJAGICgJgAgJgAICYAiAkAYgIAYgKAmAAgJgCICQCICQBiAoCYACAmACAmAIgJAGICgJgAgJgAICYAiAkAYgIAYgKAmAAgJgCICQCICQBiAoCYACAmACAmAIgJAGICgJgAgJgAICYAiAkAYgIAYgKAmAAgJgCICQCICQBiAoCYACAmACAmAIgJAGICgJgAgJgAICYAiAkAYgIAYgKAmAAgJgCICQCICQBiAoCYACAmACAmAIgJAKvAWiOYOvPVDdXV1ZunfF+PVg9Vf6lOWnoQE167t1Zfqz5dbZ2xfT9S7a++VT3pUICLz2Ou6XBzdW91xwyGpGGf76jur/Y4HEBMuHB7qrurS42izdXPBQXEhAuzs7qrxfckLJqvDlRXGQWICStz53A1zrm2DLMBxIQxtlafMIZl3Z5HfyAmjPXBfBpvlLXVTcYAYsJo1xjBWLuMAMSE0bYZwViXGQGICaMtGIEZgZgAICYAICYAiAkAYgKAmACAmAAgJgCICQBiAgBiAoCYACAmAIgJAIgJAGICgJgAICYAICYAiAkAYgKAmACAmAAgJgCICQBiAgBiAoCYACAmAIgJAIgJAGICgJgAICYAICYAiAkAYgKAmACAmAAgJgCICQBiAgBiAoCYACAmAIgJAIgJAGICgJgAICYAICYAiAkAYgKAmACAmAAgJgCICQBiAgBiAoCYACAmAIgJAKzM2jF/v6G6utpeLUzxHJ6vnqgec0gAvH4xuaX6QrV7CMqseKz6WbW3etLhAbAy5z/m2lIdrA4NQdkwY/O4cojog9WnHB4AF35nsqX6XfUuY2ljtb+6vPq2cQCs/M7kgJC8wt7qZmMAWFlMbq32GMeS89nX+A8qAIhJ9XmjWNYudycA42OysfqAUYx0qxEAjI7Jzmq9UYx0nREAjI7JdmMYa4cRAIyOibuS8cwIYExMAEBMABATAMQEADEBADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxASA1RWTl4xhrEmc0cuWZVXOyLqtvt+305ZkrFNz1dPmMNa/JnCbrJt1s24uACZmRnPVw9UJsxjpAdtk3azbRTtxPzJh23SyOm5pRnphrnqu+qNZjPSrCdym37gIGOlE9VvH0qrzh+rYBG7XEUsz0jNnXsB/zyyW9UT1ywncrmerH1meZd01zGjSHKwetzzL+u6Ebtc/Lc2yTlWHz8Tkx9WfzGRJX6lemNBt++ZwZ8m5jlZ3Tui2vVh93RIte1fy0wndtmNN5ju4SfBYdXzurLLcpr6vsG+4wp1UD1e353HX2U5Wn2zynrufbf9wbHHulf9Hh3PRpLqvet5SnePf1aNV8+eV9yfV+6srZnxAp6u91Zeb/I8FPtDiO6+PVBtmfN2eqz7eZD6WPN+h4Y73pmrNjK/bX6sPVf9YBeeFw9WmaqOOdLi698w5cn6JW7kftvieYFe1bcaGc7z6xXC1f6DV8/nyh6ofVAvDus1aVJ6pvj+E5J5VtN2/H463t1VXLfH7OO0eqL5RfW5Yw9VyoflU9d9qfbVuBi+0jw53aY+ffY4cd0W0s7qu2l6tneIB/We4zf7z8PNqNl9dP5yctk75gX2kxUd997T4eGs1u6S6sXr7lF/1nhxOxn+rHpyC/Vk33KksNN3/o8ipFr9MeqxlvnfzP2V3yb9jUxu8AAAAAElFTkSuQmCC"}),
  rapidjson.encode({DrawChrome = true, IconData = "iVBORw0KGgoAAAANSUhEUgAAAZMAAAG4CAYAAACAdwqUAAABhWlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TS0UqChYRcchQnSz4hThKFYtgobQVWnUwufRDaNKQpLg4Cq4FBz8Wqw4uzro6uAqC4AeIk6OToouU+L+k0CLGg+N+vLv3uHsHCPUyU82OMUDVLCMVj4nZ3IoYfEUI/ejFOAISM/VEeiEDz/F1Dx9f76I8y/vcn6NbyZsM8InEs0w3LOJ14ulNS+e8TxxmJUkhPiceNeiCxI9cl11+41x0WOCZYSOTmiMOE4vFNpbbmJUMlXiKOKKoGuULWZcVzluc1XKVNe/JXxjKa8tprtMcQhyLSCAJETKq2EAZFqK0aqSYSNF+zMM/6PiT5JLJtQFGjnlUoEJy/OB/8LtbszA54SaFYkDgxbY/hoHgLtCo2fb3sW03TgD/M3CltfyVOjDzSXqtpUWOgJ5t4OK6pcl7wOUOMPCkS4bkSH6aQqEAvJ/RN+WAvluga9XtrbmP0wcgQ10t3QAHh8BIkbLXPN7d2d7bv2ea/f0Ajn5yshGqn1YAAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAADdcAAA3XAUIom3gAAAAHdElNRQfkDBYQNAxfOQraAAAJD0lEQVR42u3bzYuddxmA4TuTTMiHiQYnURJQ29RsCmJjUUSIYO2H1KzEr2pXbuymKAjiSjeiFIrS/APWtO6k2EopilIRVCrabkSMViU0UmvSGq0laZPGRU6EaWbOO0lKPB/XBb9kIFnMeZ435z7vOZN1AXC59lT7q6VqcYYf5+nqueqJ6oS1A1y5herT1ZPVuTk7Z6ufVTe7DAAu3/bqkTmMyErnwWqzSwLg0ryhekpElp0fVuu78AsAg+6vPmwMy+wb/f74OrMAGHRj5z989px5sVPVde5MAIZ9vbrBGFa0oTqusgDDjlW7jWFV3uYCGLC1etEYxsd2wQwAxloygkE7xQRgvEUjGLRRTAC4YmICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGJiBACICQBiAoCYAICYACAmAIgJALNggxHAxFhXLVXbZvgxvlwdr05Zt5gAr59N1eeqj1UfqDbOyeP+U/VYdag64jIAuHwHqqPVuTk+r1T3TvgL2+vmfEdrPcD/wcer056A/ncerRbFZHqPD+Dh6ntPdf8cvaW1Fh+pvm0M00tM4Oo7VG02hovcVb3XGMQEGHager8xrGhd9SVjEBNg2O1GMNatTe5nJ4gJTIz9RjDW9mqvMYgJMN4uIzAjMQGulA/eh201AjEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATIwBATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAUBMABATABATAMQEADEBQEwAQEwAEBMAxAQAMQEAMQFATAAQEwDEBADEBAAxAWAKbDCCmbO+2l9dW71xxh/ryerP1W+rs1YPYsKVe2v1lerOasecPfYXqsPVN6pnXQpw9XmbazbcUv2+unsOQ9LoMd9dHakOuhxATLh0B6tHqzcZRduqhwQFxIRLs7d6sPOfk3De+uqB6hqjADFhbb42ejXOcttHswHEhAE7qk8Zw6ruyFt/ICYMuik/jTfOhupDxgBiwnjvNIJB+4wAxITx3mwEg3YaAYgJ4y0agRmBmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkAiAkAYgKAmAAgJgAgJgCICQBiAoCYAICYACAmAIgJAGICAGICgJgAICYAiAkArM2GgT/fXF1bLVWLMzyHF6u/VUddEgCvX0xurb5YHRgFZV4crb5f3VM96/IAWJvXvs21vXq4emwUlM1zNo+3jSL6dPVZlwfApd+ZbK9+Xr3LWNpSHa7eUt1rHABrvzN5QEguck91izEArC0mt1UHjWPF+Rxq+AcVAMSk+oJRrGqfuxOA4ZhsqT5oFGPdZgQA42Oyt9pkFGNdbwQA42OyZAyDdhkBwPiYuCsZZkYAAzEBADEBQEwAEBMAxAQAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwDEBAAxAQAxAUBMABATAMQEAMQEADEBQEwAEBMAEBMAxAQAMQFATABATAAQEwCmKyYvG8OgSZzRK9YylTOyt+n79+Y5cg0zWqiOm8Og5ybwe7I3e7M3O5uYnS1Uf6nOmMVYf/Q92Zu9XbU7t79O2Pf0UnXMasZf1wvVv6pfmcVYP5rA7+knXgSMdab6qWtp6vyy+re9TZ0fX/jijuqcs+I5Vm2e0AV+135WPd+Z0J1tqo7az6rnkxO6txurV+1nxfNStefCoBZGdycGc/H5zAS/GrimOmlHF51/Vu+Y4L3daUcrnl802T9h+j07WvF89bWD2lM9YzDLzn1TcHt5++h9Zvs6f86MZjLp7rOrZeeZaveE72xr9aRdLTuPrPYCYE/1hAH1avXNpuf/4dxcPW9vnaw+OiU7W1d9uTprbz1VvX1K9ratetjOOlcdHr1tu6qN1eerP8zhcE5VD1XvnsIPwHZV36pOzOHeTowe+64p3Nv+6gfV6Tnc25HqrtFzzjRZqD5R/WZO7/wfr25a6dXROHur66ulakOz6z+jD9p/Pfp6mq2vbhh9nrKj2fZC53+0/cnRK/xptrV63+itni0zvLOz1T+q31VPz8Dj2T16QbCzWpzhvZ2q/j56jnx+pb/wX1qpmPa1GxUJAAAAAElFTkSuQmCC"}),
  rapidjson.encode({DrawChrome = true, IconData = ""})
}

--Terminal ID based on LAN A core MAC Address
UUID = tostring(Crypto.Base64Encode(Network.Interfaces()[1]["MACAddress"]))

--Utility Fucntions

function flipIntBool(val)
  if val == 1 then
    val = 0
  else
    val = 1
  end
  return val
end

function sizeOf(T) 
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

--Status Functions

function updateStatus(msg, state)
  Controls["Status"].Value = state
  Controls["Status"].String = msg
  Controls["Status LED"].Color = StatusLEDColors[state+1]
  Controls["Status LED"].Value = 1
end

--Login / Connection Functions

function checkLoginInfo()
  if Controls["Server"].Choice == "" or 
  Controls["Username"].String == "" or 
  Controls["Password"].String == "" then
    return false
  else
    return true
  end
end

function setSecured() --set seceret password
  Controls["Secured"].String = Controls["Password"].String
  hidePassword()
end

function startConnection()
  if checkLoginInfo() then
    device_count = 0
    discovery = true
    clear_status = false
    SmartPlugsAlias_Unsorted = {}
    SmartPowerStripsAlias_Unsorted = {}
    SmartBulbsAlias_Unsorted = {}
    Devices = {
      SmartPlugs = {},
      SmartPlugsState = {},
      SmartPowerStrips = {},
      SmartPowerStripPlugs = {},
      SmartBulbs = {},
      SmartBulbsState = {}
    }
    server = Controls["Server"].Choice
    enableUI("Login", false, nil)
    getToken()
    Update:Start(5)
    RenewAuth:Start(2592000)
    updateStatus("", 5)
  else
    updateStatus("Invalid credentials", 2)
    Controls["Connect"].Value = 0
    Controls["Username"].IsDisabled = false
    Controls["Password"].IsDisabled = false
    Update:Stop(1)
  end
end

function stopConnection()
  Controls["Connect"].Value = 0
  for c, ctl in pairs (Controls["Smart Plug"]) do
    Update:Stop()
    enableUI("Smart Plugs", false)
    enableUI("Smart Power Strips", false)
    enableUI("Smart Power Strip Plugs", false)
    enableUI("Smart Bulbs", false)
    enableUI("Login", true)
  end
end


function connect(ctl)
  if ctl.Value == 1 then
    startConnection()
  else
    updateStatus("Offline", 3)
    stopConnection()
  end
end

--API Parse Functions

function parseJSONResponseTEST(tbl, code, data, e)
print(tbl["Data"])
print(data)
end

function parseJSONResponse(tbl, code, data, e)
  data = string.gsub(data, "\\u2019", "'")
  data = string.gsub(data, "\\u201d", "''")
  --print(code)
  if code == 200.00 then
    xdata = rapidjson.decode(data)
    xtbl = rapidjson.decode(tbl["Data"])
    xresult = xdata["result"]

    if xtbl["method"] == "login" then
      if xdata["error_code"] == 0 then
        APIToken = xresult["token"]
        print("login - success")
        if discovery then
          discovery = false
          getDeviceList()
          updateStatus("Discovering Devices", 5)
        end
      else
        updateStatus(xdata["msg"], 2)
        stopConnection()
      end
    elseif xtbl["method"] == "getDeviceList" then 
      parseDevices(xdata)
    elseif xtbl["method"] == "passthrough" then
      parsePassthrough(xdata, xtbl)
    else
      print(xtbl["method"].." - not defined")
    end
    if xdata["error_code"] == "-20571" then
      disableDeviceOnError(xdata, xtbl)
    end
  end
end

function disableDeviceOnError(xdata, xtbl)
  for k,v in pairs (Devices) do
    for kk,vv in pairs (v) do
      if type(vv) == "table" then
        if vv["deviceId"] == xtbl["params"]["deviceId"] then
          if k == "SmartPlugs" then
            if vv["alias"] then
              Controls["Smart Plug"][kk].Legend = vv["alias"].."\n(Offline)"
            end
            Controls["Smart Plug"][kk].IsDisabled = true
            Controls["Smart Plug RSSI"][kk].IsDisabled = true
            Controls["Smart Plug RSSI"][kk].Style = rssi_bars[5]
          elseif k == "SmartBulbs" then
            if vv["alias"] then
              Controls["Smart Bulb"][kk].Legend = vv["alias"].."\n(Offline)"
            end
            Controls["Smart Bulb"][kk].IsDisabled = true
            Controls["Smart Bulb RSSI"][kk].IsDisabled = true
            Controls["Smart Bulb RSSI"][kk].Style = rssi_bars[5]
          elseif k == "SmartPowerStrips" then
            --print("test")
            Controls["Smart Power Strip"][kk].IsDisabled = true
          end
        end
      end 
    end
  end
end

function setRSSIIcon(rssi, idx, ctl_type)
  if rssi < -80 then
    Controls[ctl_type][idx].Style = rssi_bars[1]
  end
  if rssi > -80 and rssi < -60 then
    Controls[ctl_type][idx].Style = rssi_bars[2]
  end
  if rssi > -60 and rssi < -40 then
    Controls[ctl_type][idx].Style = rssi_bars[3]
  end
  if rssi > -40 then
    Controls[ctl_type][idx].Style = rssi_bars[4]
  end
end

plug_count = 0

function parseSysinfo(api_response)
  if string.find(api_response["model"], "KL130") then
    for k,v in pairs (Devices.SmartBulbs) do
      if v["deviceId"] == api_response["deviceId"] then
        Controls["Smart Bulb"][k].IsDisabled = false
        Controls["Smart Bulb RSSI"][k].IsDisabled = false
        Controls["Smart Bulb Kelvin"][k].IsDisabled = false
        Controls["Smart Bulb Value"][k].IsDisabled = false
        Controls["Smart Bulb Kelvin K"][k].IsDisabled = false
        Controls["Smart Bulb"][k].Value = api_response["light_state"]["on_off"]
        Controls["Smart Bulb"][k].Legend = api_response["alias"]
        Devices.SmartBulbs[k] = api_response
        setRSSIIcon(api_response["rssi"], k, "Smart Bulb RSSI")
        if api_response["light_state"]["on_off"] == 1 then
          hue = api_response["light_state"]["hue"]
          sat = api_response["light_state"]["saturation"]
          val = api_response["light_state"]["brightness"]
          kelvin = api_response["light_state"]["color_temp"]
          for k,v in pairs (Devices.SmartBulbs) do
            if v["deviceId"] == api_response["deviceId"] then
              if kelvin == 0 then
                color = rgbToHex(hsvToRgb(hue/360, sat/100, val/100, 1))
                Controls["Smart Bulb"][k].Color = color
                Controls["Smart Bulb Color"][k].String = color
                Controls["Smart Bulb Hue"][k].Value = hue
                Controls["Smart Bulb Saturation"][k].Value = sat
                Controls["Smart Bulb Value"][k].Value = val
                Controls["Smart Bulb Kelvin K"][k].String = "Auto"
                enableUI("Smart Bulbs HSV", true, k)
              else
                Controls["Smart Bulb Kelvin K"][k].String = kelvin.." K"
                Controls["Smart Bulb Kelvin"][k].Value = math.floor(kelvin/900)*10
                Controls["Smart Bulb"][k].Color = rgbToHex(kelvinToRGB(kelvin))
                enableUI("Smart Bulbs HSV", false, k)
              end
            end
          end
      else
        for k,v in pairs (Devices.SmartBulbs) do
          if v["deviceId"] == api_response["deviceId"] then
            Controls["Smart Bulb"][k].Color = ""
            Devices.SmartBulbsState[k] = 0
          end
        end
        for k,v in pairs (Devices.SmartBulbs) do
          if v["deviceId"] == api_response["deviceId"] then
            Controls["Smart Bulb"][k].Legend = api_response["alias"]
            Controls["Smart Bulb"][k].IsDisabled = false
            Controls["Smart Bulb RSSI"][k].IsDisabled = false
            setRSSIIcon(tonumber(api_response["rssi"]), k, "Smart Bulb RSSI")
          end
        end
      end
      end
    end
  elseif string.find(api_response["model"], "KP303") then 
    for k,v in pairs (Devices.SmartPowerStrips) do
    strip_count = k
      if v["deviceId"] == api_response["deviceId"] then
        Controls["Smart Power Strip"][k].String = string.sub(api_response["alias"],1)
        Controls["Smart Power Strip"][k].IsDisabled = false
        for kk, vv in pairs (api_response["children"]) do
          last_plug = strip_count*3; first_plug = last_plug-2
          for plug_idx = first_plug, last_plug do
            if kk + (strip_count-1)*3 == plug_idx then
              setRSSIIcon(tonumber(api_response["rssi"]), plug_idx, "Smart Power Strip RSSI")
              Controls["Smart Power Strip RSSI"][plug_idx].IsDisabled = false
              Controls["Smart Power Strip Plug"][plug_idx].Legend = vv["alias"]
              Controls["Smart Power Strip Plug"][plug_idx].IsDisabled = false
              Controls["Smart Power Strip Plug"][plug_idx].Value = vv["state"]
              if vv["state"] == 1 then
                Controls["Smart Power Strip Plug"][plug_idx].Color = "#6dd5e2"
              else
                Controls["Smart Power Strip Plug"][plug_idx].Color = ""
              end
              Devices.SmartPowerStripPlugs[plug_idx] = vv
              --print(vv["alias"], vv["state"])
            end
          end
        end
      end
    end
  elseif string.find(api_response["model"], "HS10") then
    for k,v in pairs (Devices.SmartPlugs) do
      if v["deviceId"] == api_response["deviceId"] then
        Controls["Smart Plug"][k].Value = api_response["relay_state"]
        Controls["Smart Plug"][k].Legend = api_response["alias"]
        Devices.SmartPlugs[k]["relay_state"] = api_response["relay_state"]
        Controls["Smart Plug"][k].IsDisabled = false
        Controls["Smart Plug RSSI"][k].IsDisabled = false
        setRSSIIcon(tonumber(api_response["rssi"]), k, "Smart Plug RSSI")
        if api_response["relay_state"] == 1 then
          Controls["Smart Plug"][k].Color = "#6dd5e2"
        else
          Controls["Smart Plug"][k].Color = ""
        end
      end
    end
  end
end

function parseRelayState(api_response, api_request, api_params)
--[[for k,v in pairs (api_params) do print(k,v) end
  if api_response["err_code"] == 0 then
    for k,v in pairs (Devices.SmartPlugs) do
      if v["deviceId"] == api_params["deviceId"] then
        Controls["Smart Plug"][idx].Value = api_request["system"]["set_relay_state"]["state"]
        Controls["Smart Plug"][idx].IsDisabled = false
        if api_request["system"]["set_relay_state"]["state"] == 1 then
          Controls["Smart Plug"][k].Color = "#6dd5e2"
        else
          Controls["Smart Plug"][k].Color = ""
        end
      end
    end
  end]]
  clear_status = true
end

function parsePassthrough(xdata, xtbl)
  if xdata["result"] then
    for k,v in pairs (xdata["result"]) do
      xdata["result"]["responseData"] = rapidjson.decode(xdata["result"]["responseData"])
    end
  
    for k,v in pairs (xdata["result"]["responseData"]) do
      api_module = k
      api_module_response = v
    end
  
    for k,v in pairs (api_module_response) do 
      api_method = k
      api_response = v
      --print("api: "..api_module.." -> "..api_method)
    end
  end

  if api_module == "system" then
    if api_method == "get_sysinfo" then parseSysinfo(api_response) end
    if api_method == "set_relay_state" then parseRelayState(api_response, rapidjson.decode(xtbl["params"]["requestData"]), xtbl["params"]) end
  end
  
end  

function hsvToRgb(h, s, v, a)
  local r, g, b

  local i = math.floor(h * 6);
  local f = h * 6 - i;
  local p = v * (1 - s);
  local q = v * (1 - f * s);
  local t = v * (1 - (1 - f) * s);

  i = i % 6

  if i == 0 then r, g, b = v, t, p
  elseif i == 1 then r, g, b = q, v, p
  elseif i == 2 then r, g, b = p, v, t
  elseif i == 3 then r, g, b = p, q, v
  elseif i == 4 then r, g, b = t, p, v
  elseif i == 5 then r, g, b = v, p, q
  end

  return { math.floor(r * 255), math.floor(g * 255), math.floor(b * 255) }
end

function rgbToHex(rgb)
	local hexadecimal = '#'

	for key, value in pairs(rgb) do
		local hex = ''

		while(value > 0)do
			local index = math.fmod(value, 16) + 1
			value = math.floor(value / 16)
			hex = string.sub('0123456789ABCDEF', index, index) .. hex			
		end

		if(string.len(hex) == 0)then
			hex = '00'

		elseif(string.len(hex) == 1)then
			hex = '0' .. hex
		end

		hexadecimal = hexadecimal .. hex
	end
	return hexadecimal
end

function kelvinToRGB(kelvin)
    local temp = kelvin / 100;
    local red, green, blue
    if temp <= 66 then 
        red = 255; 
        green = temp;
        green = 99.4708025861 * math.log(green) - 161.1195681661;
        if temp <= 19 then

            blue = 0;
            else
            blue = temp-10;
            blue = 138.5177312231 * math.log(blue) - 305.0447927307;
        end
        else
        red = temp - 60;
        red = 329.698727446 * math.pow(red, -0.1332047592);
        green = temp - 60;
        green = 288.1221695283 * math.pow(green, -0.0755148492 );
        blue = 255;
      end
      return {math.floor(red), math.floor(green), math.floor(blue)}
    end

function parseDevices(xdata)
  for k, v in pairs (xdata["result"]["deviceList"]) do
    if string.find(v["deviceModel"], "KL130") then
      table.insert(SmartBulbsAlias_Unsorted, v["alias"])
    elseif string.find(v["deviceModel"], "HS10") then
      table.insert(SmartPlugsAlias_Unsorted, v["alias"])
    elseif string.find(v["deviceModel"], "KP303") then
      table.insert(SmartPowerStripsAlias_Unsorted, v["alias"])
    end
  end
  
  Devices.SmartPlugs = sortDevicesAToZ(SmartPlugsAlias_Unsorted, xdata["result"]["deviceList"])
  Devices.SmartPowerStrips = sortDevicesAToZ(SmartPowerStripsAlias_Unsorted, xdata["result"]["deviceList"])
  Devices.SmartBulbs = sortDevicesAToZ(SmartBulbsAlias_Unsorted, xdata["result"]["deviceList"])
  
  for k,v in pairs (Devices) do
    for kk,vv in pairs (v) do
      if k == "SmartPlugs" then Controls["Smart Plug"][kk].Legend = "Plug" end
      if k == "SmartBulbs" then Controls["Smart Bulb"][kk].Legend = "Bulb" end
      if k == "SmartPowerStrips" then Controls["Smart Power Strip"][kk].String = "Power Strip" end
    end 
  end
  
  strip_count = #Devices.SmartPowerStrips
  --print(strip_count)
  for i=1, strip_count*3 do
    Controls["Smart Power Strip Plug"][i].Legend = "Plug"
  end
  
  clear_status = true
end

function sortDevicesAToZ(tblA, tblB)
  local t = {}
  table.sort(tblA, function(a, b) return a:upper() < b:upper() end)
  
  for k,v in pairs (tblA) do
    for kk,vv in pairs (tblB) do
      if vv["alias"] == v then
        table.insert(t, vv)
      end
    end
  end
  
  return t
end

function getToken()
  local payload = {
    method = "login",
      params = {
        appType = "Kasa_Android",
        cloudUserName = Controls["Username"].String,
        cloudPassword = Controls["Secured"].String,
        terminalUUID = UUID, 
      }
  }

  HttpClient.Upload {
    Url = "https://wap.tplinkcloud.com",
    Method = "POST",
    Data = rapidjson.encode(payload),
    EventHandler = parseJSONResponse,
    Timeout = 10,
    Headers = {
      ["Content-Type"] = "application/json",
    },
  }
end

function getDeviceList()
  local payload = {
    method = "getDeviceList"
  }
  HttpClient.Upload {
    Url = string.format("%s?token=%s", server, APIToken),
    Method = "POST",
    Data = rapidjson.encode(payload),
    EventHandler = parseJSONResponse,
    Timeout = 10,
    Headers = {
      ["Content-Type"] = "application/html",
    },
  }
end

--API Functions

function setState(selectedDevice, idx, state)
  local payload = {
    method = "passthrough",
      params = {
        deviceId = selectedDevice,
        requestData = rapidjson.encode({system = {set_relay_state = {state = state}}})
      }
  }
  
  HttpClient.Upload {
    Url = string.format("%s?token=%s", server, APIToken),
    Method = "POST",
    Data = rapidjson.encode(payload),
    EventHandler = parseJSONResponse,
    Timeout = 10,
    Headers = {
      ["Content-Type"] = "application/html",
    },
  }
  if state == 1 then
    updateStatus('Turning on Smart Plug "'..Devices.SmartPlugs[idx]["alias"]..'"', 0)
  else
    updateStatus('Turning off Smart Plug "'..Devices.SmartPlugs[idx]["alias"]..'"', 0)
  end
end

apiModules = {
    system = 'smartlife.iot.common.system',
    cloud = 'smartlife.iot.common.cloud',
    schedule = 'smartlife.iot.common.schedule',
    timesetting = 'smartlife.iot.common.timesetting',
    emeter = 'smartlife.iot.common.emeter',
    netif = 'netif',
    lightingservice = 'smartlife.iot.smartbulb.lightingservice',
  }
  
function setSmartBulbHSV(ctl)
  ctl_group, idx = matchControl(ctl.Index)
  selectedDevice = Devices.SmartBulbs[idx]["deviceId"]
  hue = Controls["Smart Bulb Hue"][idx].Value
  sat = Controls["Smart Bulb Saturation"][idx].Value
  val = Controls["Smart Bulb Value"][idx].Value
  kelvin = Controls["Smart Bulb Kelvin"][idx].Value
  if kelvin == 0 then
    enableUI("Smart Bulbs HSV", true, idx)
    Controls["Smart Bulb"][idx].Color = rgbToHex(hsvToRgb(hue/360, sat/100, val/100, 1))
    Controls["Smart Bulb Kelvin K"][idx].String = "Auto"
  else
    --print(kelvin)
    enableUI("Smart Bulbs HSV", false, idx)
    --Controls["Smart Bulb"][idx].Color = rgbToHex(kelvinToRGB(kelvin))
    Controls["Smart Bulb Saturation"][idx].Value = 100
    Controls["Smart Bulb Value"][idx].Value = 100
  end
  
  local payload = {
    method = "passthrough",
      params = {
        deviceId = selectedDevice,
        requestData = rapidjson.encode({["smartlife.iot.smartbulb.lightingservice"] = { transition_light_state = { 
          transition_period = 100,
          mode = "normal",
          hue = tonumber(Controls["Smart Bulb Hue"][idx].String),
          saturation = tonumber(Controls["Smart Bulb Saturation"][idx].String),
          brightness = tonumber(Controls["Smart Bulb Value"][idx].String),
          color_temp = tonumber(Controls["Smart Bulb Kelvin"][idx].String)*100,
          ignore_default = 1
          }}}) 
      }
  }
  
  HttpClient.Upload {
    Url = string.format("%s?token=%s", server, APIToken),
    Method = "POST",
    Data = rapidjson.encode(payload),
    EventHandler = parseJSONResponse,
    Timeout = 10,
    Headers = {
      ["Content-Type"] = "application/html",
    },
  }
end

function setStateBulb(selectedDevice, idx, state)
  --print(selectedDevice, state)
  local payload = {
    method = "passthrough",
      params = {
        deviceId = selectedDevice,
        requestData = rapidjson.encode({["smartlife.iot.smartbulb.lightingservice"] = { transition_light_state = { on_off = state }}}) 
        --requestData = ""
      }
  }
  
  HttpClient.Upload {
    Url = string.format("%s?token=%s", server, APIToken),
    Method = "POST",
    Data = rapidjson.encode(payload),
    EventHandler = parseJSONResponse,
    Timeout = 10,
    Headers = {
      ["Content-Type"] = "application/html",
    },
  }
  
end


function setStatePS(selectedDevice, idx, state)
  --print(selectedDevice, state, idx)
  id_string = Devices.SmartPowerStripPlugs[idx]["id"]
  local payload = {
    method = "passthrough",
      params = {
        deviceId = selectedDevice,
        requestData = rapidjson.encode({context = {child_ids = {id_string}},system = {set_relay_state = {state = state}}})
      }
  }
  
  HttpClient.Upload {
    Url = string.format("%s?token=%s", server, APIToken),
    Method = "POST",
    Data = rapidjson.encode(payload),
    EventHandler = parseJSONResponse,
    Timeout = 10,
    Headers = {
      ["Content-Type"] = "application/html",
    },
  }
  
  if state == 1 then
    updateStatus('Turning on Smart Power Strip Plug "'..Devices.SmartPowerStripPlugs[idx]["alias"]..'"', 0)
  else
    updateStatus('Turning off Smart Power Strip Plug "'..Devices.SmartPowerStripPlugs[idx]["alias"]..'"', 0)
  end
end

function toggleSmartPlug(ctl)
  ctl_group, idx = matchControl(ctl.Index)
  Controls["Smart Plug"][idx].Color = ""
  Controls["Smart Plug"][idx].IsDisabled = true
  Timer.CallAfter(function() setState(Devices.SmartPlugs[idx]["deviceId"], idx, flipIntBool(Devices.SmartPlugs[idx]["relay_state"])) end, 1)
end

function toggleSmartBulb(ctl)
  ctl_group, idx = matchControl(ctl.Index)
  Controls["Smart Bulb"][idx].Color = ""
  Controls["Smart Bulb"][idx].IsDisabled = true
  --for k,v in pairs (Devices.SmartBulbs[idx]["light_state"]) do print(k,v) end
  Timer.CallAfter(function() setStateBulb(Devices.SmartBulbs[idx]["deviceId"], idx, flipIntBool(Devices.SmartBulbs[idx]["light_state"]["on_off"])) end, 1)
end

function toggleSmartPowerStrip(ctl)
  ctl_group, idx = matchControl(ctl.Index)
  --print(ctl_group, idx)
  strip_count = #Devices.SmartPowerStrips
  --print(idx)
  for i=1, strip_count do
    last_plug = i*3; first_plug = last_plug-2
    --print(first_plug, last_plug)
    if first_plug <= idx and last_plug >= idx then ps_idx = i end
  end
  --print(ps_idx, idx)
  Controls["Smart Power Strip Plug"][idx].Color = ""
  Controls["Smart Power Strip Plug"][idx].IsDisabled = true
  Timer.CallAfter(function() setStatePS(Devices.SmartPowerStrips[ps_idx]["deviceId"], idx, flipIntBool(Devices.SmartPowerStripPlugs[idx]["state"])) end, 1)
end

function getDeviceInfo(selectedDevice, devType)
--print("HTTP POST: getDeviceInfo()")
--print(selectedDevice)

  local payload = {
    method = "passthrough",
      params = {
        deviceId = selectedDevice,
        requestData = "{\"system\":{\"get_sysinfo\":\"null\"}}"
      }
  }

  HttpClient.Upload {
    Url = string.format("%s?token=%s", Controls["Server"].Choice, APIToken),
    Method = "POST",
    Data = rapidjson.encode(payload),
    EventHandler = parseJSONResponse,
    Timeout = 3,
    Headers = {
      ["Content-Type"] = "application/html",
    },
  }
end

count = 0

function pollServer()
if clear_status then
  updateStatus("", 0)
  clear_status = false
end
  if #Devices.SmartPlugs > 0 then 
    for k,v in pairs (Devices.SmartPlugs) do 
      getDeviceInfo(v["deviceId"], nil)
    end
  end
  if #Devices.SmartPowerStrips > 0 then 
    for k,v in pairs (Devices.SmartPowerStrips) do 
      getDeviceInfo(v["deviceId"], nil)
      --print(v["deviceId"])
    end
  end
  if #Devices.SmartBulbs > 0 then 
    for k,v in pairs (Devices.SmartBulbs) do 
      getDeviceInfo(v["deviceId"], "Bulb")
    end
  end
  count = 0
if Controls["Connect"].Value == 0 then Controls["Connect"].Value = 1 end
end

--UI Functions

function hidePassword() --hid secret password
  if Controls["Secured"].String ~= "" then
    Controls["Password"].String = "******"
  else
    Controls["Secured"].String = nil
  end
end

control_list = {}
function addControl(c, ctl, name)
  control_list[ctl.Index] = {}
  control_list[ctl.Index][name] = c
end

function matchControl(idx)
  for k,v in pairs (control_list[idx]) do
    ctl_group = k
    idx = v
  end
  return ctl_group, idx
end

function enableUI(ctl_group, state, idx)
  if idx == nil then
    if ctl_group == "Smart Plugs" then
      for c, ctl in pairs (Controls["Smart Plug"]) do
        ctl.Color = ""; ctl.Legend = ""; ctl.Value = 0; ctl.IsDisabled = not state
      end
      for c, ctl in pairs (Controls["Smart Plug RSSI"]) do
        ctl.Style = rssi_bars[5]; ctl.IsDisabled = not state
      end
    end
    if ctl_group == "Smart Power Strips" then
      for c, ctl in pairs (Controls["Smart Power Strip"]) do
        ctl.Color = ""; ctl.String = ""; ctl.Value = 0; ctl.IsDisabled = not state
      end
      for c, ctl in pairs (Controls["Smart Power Strip RSSI"]) do
        ctl.Style = rssi_bars[5]; ctl.IsDisabled = not state
      end
    end
    if ctl_group == "Smart Power Strip Plugs" then
      for c, ctl in pairs (Controls["Smart Power Strip Plug"]) do
        ctl.Color = ""; ctl.Legend = ""; ctl.Value = 0; ctl.IsDisabled = not state
      end
    end
    if ctl_group == "Smart Bulbs" then
      for c, ctl in pairs (Controls["Smart Bulb"]) do
        ctl.Color = ""; ctl.Legend = ""; ctl.Value = 0; ctl.IsDisabled = not state
      end
      for c, ctl in pairs (Controls["Smart Bulb Hue"]) do
        ctl.Value = 0; ctl.IsDisabled = not state
      end
      for c, ctl in pairs (Controls["Smart Bulb Saturation"]) do
        ctl.Value = 0; ctl.IsDisabled = not state
      end
      for c, ctl in pairs (Controls["Smart Bulb Value"]) do
        ctl.Value = 0; ctl.IsDisabled = not state
      end
      for c, ctl in pairs (Controls["Smart Bulb Kelvin"]) do
        ctl.Value = 0; ctl.IsDisabled = not state
      end
      for c, ctl in pairs (Controls["Smart Bulb Color"]) do
        ctl.String = ""; ctl.IsDisabled = not state
      end
      for c, ctl in pairs (Controls["Smart Bulb Kelvin K"]) do
        ctl.Style = rssi_bars[5]; ctl.IsDisabled = not state
      end
      for c, ctl in pairs (Controls["Smart Bulb RSSI"]) do
        ctl.Style = rssi_bars[5]; ctl.IsDisabled = not state
      end
    end
    if ctl_group == "Login" then
      Controls["Connect"].Value = 0
      Controls["Username"].IsDisabled = not state
      Controls["Password"].IsDisabled = not state
      Controls["Server"].IsDisabled = false
    end
  else
    if ctl_group == "Smart Bulbs HSV" then
      Controls["Smart Bulb Hue"][idx].IsDisabled = not state
      Controls["Smart Bulb Saturation"][idx].IsDisabled = not state
      Controls["Smart Bulb Color"][idx].IsDisabled = not state
    end
  end
end

--Init

enableUI("Smart Plugs", false)
enableUI("Smart Power Strips", false)
enableUI("Smart Power Strip Plugs", false)
enableUI("Smart Bulbs", false)
enableUI("Login", true)
if checkLoginInfo() then
  startConnection()
else 
  updateStatus("Enter credentials and click connect", 3)
end

--EventHandlers

Controls["Password"].EventHandler = setSecured
Controls["Connect"].EventHandler = connect
Update.EventHandler = pollServer
RenewAuth.EventHandler = getToken

for c, ctl in pairs (Controls["Smart Plug"]) do
  ctl.EventHandler = toggleSmartPlug
  addControl(c, ctl, "Smart Plug")
end

for c, ctl in pairs (Controls["Smart Bulb"]) do
  ctl.EventHandler = toggleSmartBulb
  addControl(c, ctl, "Smart Bulb")
end

for c, ctl in pairs (Controls["Smart Power Strip Plug"]) do
  ctl.EventHandler = toggleSmartPowerStrip
  addControl(c, ctl, "Smart Power Strip Plug")
end

for c, ctl in pairs (Controls["Smart Bulb Hue"]) do
  ctl.EventHandler = setSmartBulbHSV
  addControl(c, ctl, "Smart Bulb Hue")
end

for c, ctl in pairs (Controls["Smart Bulb Saturation"]) do
  ctl.EventHandler = setSmartBulbHSV
  addControl(c, ctl, "Smart Bulb Saturation")
end

for c, ctl in pairs (Controls["Smart Bulb Value"]) do
  ctl.EventHandler = setSmartBulbHSV
  addControl(c, ctl, "Smart Bulb Value")
end

for c, ctl in pairs (Controls["Smart Bulb Kelvin"]) do
  ctl.EventHandler = setSmartBulbHSV
  addControl(c, ctl, "Smart Bulb Kelvin")
end

--]=]