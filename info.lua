PluginInfo = {
    Name = "TPLink~TPLink Kasa",
    Version = "0.1",
    BuildVersion = "1.0.0.1",
    Id = "1600d89b-0f36-4acd-b1b8-728db7967c03",
    Author = "Ethan Klein",
    Description = "Cloud API control of TPLink Kasa Devices",
}