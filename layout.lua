local CurrentPage = pagenames[props["page_index"].Value]

local KasaLogo = "iVBORw0KGgoAAAANSUhEUgAAAPoAAABMCAYAAABEf6pVAAAXsHpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarZpXklw5kkX/sYpZArRYDgAHzHoHs/w5F5HJIqtY1d1mw7QUDPEe4OIKR7jzv/+67n/4V2r2LpfW66jV8y+PPOLkj+4//8b7GXx+P9+/O7+eC78+7vL+eiLyUOJ3+vy3fb0hTB4vf7zh+x5h/fq461/PxP51oa8nvi+YdOfIH/bzInk8fh4P+etC43z+qKO3n5e64uf3/nrhW8rX993xXa+sr+3yf/fzA7kRJSvcKMV4Ukj+/cyfFaTP9+Q78zOmwetCqvydUnP8yil8rYSA/LK979/e/xygX4L8/Zf7c/Sz/T74cX69Iv0plvUrRvzx2ydC+X3wX4h/unH6saL46xMWvvPwmyBf6/eez+5mrkS0flWUd9/RedG/Rthzem+rfDW+C3+39zX46n76za3Mb7/42mGESFauCzlYmOGG837vsFlijic2fse4Y3qP9dTiiDspT1lf4caWRrLUyd+Ox5G6nOKPtYR33/Hut0PnzhZ4aQxcLPCWv/1y//Tkf/Pl7lWzhaBgkvrwSXBUXbMMZU4/eRUpCPcrb+UF+PvrK/3+p8KiVMlgeWHubHD69bnEKuGP2kovz4nXFX5/chxcs68LsCDuXVhMSGTA15BKqMG3GBslkWInQZOVx5TjIgOhlGgsMuaUanQt9qh7854W3mtjiTXqYbCJRBS6qZGbkSbJyrlQPy13amiWVHIppZZWuiujzJpqrqXW2qpAbrbUciutttZ6G2321HMvvfbWex99jjgSGFhGHW30Mcac0U1uNLnW5PWTR1ZcaeVVVl1t9TXW3JTPzrvsutvue+xp0ZIBE1atWbdh8wR3QIqTTzn1tNPPOPNSazfdfMutt91+x50/svaV1b98/RdZC19Ziy9Tel37kTUeda19XyIITopyRsZiDmS8KQMUdFTOfA85R2VOOfMj0hQlssii3DgLyhgpzCfEcsOP3P2Ruf8ob670/yhv8d9lzil1/x+Zc6Tur3n7TdZMPLdfxj5dqJj6RPfxmhm749v7uY+VC+e2PFe1Y+yvrHNyG4e8gZt1jnpvySsbHdJs1rwDgdPVdSNzcYGehLnbjsXGshTDOMbyCpnjBrdOm4OoEvhTrZ+w64m15DnWrW2ucgkjMUoEtKVybK990xrE+7L9XuoksvPuNVlnjMPGJG5pVz/2yLeFe0LpA/EwA1sjlYvYX4DyLMuzlWQzl0PgVy5xZgtxD5ietAaQM80wmzUoMNQ1L1vUTfJwdZWadr/n3NXnuTxK9/dRczfBsd+9BjtrHy48LNVmVFwKDTIH2lc7szcL2ZVQRrAaCjHb2cYh6ZdYsyeDc27IfdVuq4DdQBGv3aDGKLu3XZISMajjEx0oOm9Od280StubxpzrHvZYeyFPZqQt+zrvSnMhBW4vPB4LlQZnzEM60EzJlQYM0HhaWToxksEzCmG0WLMR3bpGoIAGVH7K2KqZc+qixi/11zoJHKEmp1UH8haDesZYaB6H8mBjiC3tniAVyAxCmtwMIdJheb47JX5jMu7UbnCp7dwucEETGiSpgsmBniS1tnLm+dFB7fG2XpqkIo3RjlFuYXdqMcbVzKVKVa2auhmCB7lz06Z2aKNGbYd5+2KjMHE2bhdowktfn9E7SyzQL/sVozpySVmEfi3rli1kohUoza2Nrm1pzwEQUQr7HHFgsj2mksVqEk3bThq3s7U8FmVjJ/GLOir9LrjmFD9uNivshmVdO/6um2gmC2uuQKHRwqfGenK+czng1NOMYAnIMmhZWtDKmMXOWG14I6WHlr/smobhixWAVGr1CMKQ5dZv26zosJsa5qQrQm4QKBIEPbEQQLuqv1sDBso8nsKnACtb8nunVnh5JbxtteAd+EsUwHAaoh9k0ARf9uyEglKBZ9ImvoHHaNKYFy02fLkD0INZq6qubZSLQxMjdcdJnRy09inNU8vk+dIXyZ1nldzjaTusNqegKC+iDLw/CcSjZtWpHVswD5L42wcB6Kevyd5moSiNxm7c9FbElG/gEugNPPYdlz+7jRfiNLf7FRrvD2g8i4DwYCA6vUwDgxTBPWe3OCTh7mmzNJYfC/XlVmcpQPwYfa9Ar+oim1BRRhkQGHlRGb0FveThE6hHu0M6xr9thBoeYEVUJg2eL0jC/kEY1jzZSrODTIlGn4P/tFBLBIznSFqOaKMIkJ1ahzVgauJFLCRrICRbnBbKysRMAL/3ogtXp3B3W2HSV3YpdgJxYt9nXEB0HYMluy+u00EATFlkgUoeaVtfEcSHm0p9wBlm79sm3RtVRyytTcswdqdJZ1qtFFsO/Q1Dl1X+hMG/QvAulP6hMNhPbYmaYDcABURuECAF50oeaDQUQQWdN1zUW3voDkhQHhdgPBUkBzKuDfqH3XqeB/1MIWvFU8+hOISG7+dSgwZpH9WGFaE7EAAaDXgR47X+gtE/QXQjMtt9Pf/jWXrx63n2UMJNAHmcnwsgzegVazCf2BdKWjD8DQY4umqhIfMJ2rEFyYJz0FkNUMfFM6vdYfd5h19ZzA2aXsCVeBJW9AZ1szN20wF2aCDyTAFf0LW+zEC8XM1oeii9jnMQPly2JnEwvpnGQIRUInKRY5stOJwNCmcfiZcFaMZNewd2Cr3gmM/Y/oFH0dKAnK7ehfo8MmvMXRdIZXamA+BDh0DvHqwRIAmHumNB3KxnPBPKBbQDvgckCVy2AdjfxU3bpT2oU5/Rm+5MblQKcodVL7jFImwED5IDaQ3orxOqYw29TzSom5kRbfBZI5VUAm+LqTtgnnIXoBW02J3l7mQCMiAFomoSctbzvdWEvFkUSN8GGCWQnsEeJFC4UG0ohQwc3Fr3ShmkyzdukBdJ6nkE54dGW8DuphwgamIIpGsldPLZiccmIoL7jrS4W9OUYsh0TriW+plV44cKfVHeuUiqQbowXM4wIP0/St0dwwgKeXQ2xGvCJ3gj5euRSYH+A2fORs964VTlposOYkMRZyK2op0ABmps8nA6KDZyi0IFogURsALBDdGkxjYN//AX7KRc5k6osk4JwoKYXgh5k054hzqE11g3qcF/oDuzD+dQu3WxnQaInuvB48EqAAGyMzLlsuRwt3RoAZZFJuE2x5vICdCIzBr4CWoGJVzW5v10/QxjNk/C7W4Mfzrgj0VCvPchqX3Q0AEmO25vBFyjqIDTkdRY4HvRlWlWU7JHDbTzon1ORs1gxW3DWYO3o3BoURMLuo1aAmpgFzoLlJ7Q/Z4FCsBWhIpQRm0IOLlsRBgJAGJaGCBEFzzWwGEUVXTcnOBAYVhVg4ZnJhVoABC6sVIcKrqCtE2RDM2GTmpQr4euIrVKgikDvK2L9PsScFJI20ZNUHSvEXsc/QViuBwCiXizXhNsogehSi4Hcx+AAbJV7bgifRjJ/VAwIU0yhwr1cH7gphk9dkkeWEAdwlmURNeNKpxLQY6CbWlIKHeh5wYDiTUQoUQOmYjECxMGzInXT3rZ5I2QClAU6cbP7BDVo4Ac+AwCASO9rgDNCfKLNHSq5+AZM3UYIQu6AVcI0cEPOLL4R/eRqoP/oN+Qt8Eh3TYLgG4IONo+VlgPR4aS5A3q3nHoLF70adO4NdO7aEHEZEVuIMfpYY19Xgv61Lg9gvmNLBO93qjG04GlcdXzySh62QK8idHwJUJEKHtC3VFmDcrO7IXq1SgIDhSsImZhTOl6FVLjISqAUmMzlB38DQGInoGGWTwhBF6d3/KJ1P2Ul4xKPvxJYaJ7y/LwJ2L6jt4iWugiF4HVE+lP+Wi82Ln5qCAcC0aBYLVCCJhnilyDq4MWvI3LYQsnhbanSgEwwOVeJDYITuntLgoq9AHcb5u2RAvtMIFMbBILAos6ZUin0ZwLiBc0YkseLZJIMAKQpHzwD1AdmjxfNCSeAfULJiPzUqWqBJh7iOgpENwWTZUP/Ib5kfkkmAQB5VE3rv8uKsVadChLGSqUGOVkGrOiO1OCqiOtz4aAodOlFAkMmDSUrC17i0IYiCLKuQAkjnDh6yZcT09IbxBlYAMA52mgfD09EOapkmoFUwwfJdAj1YHsR9tMELeyNXK3sTC1oRgbQYek0FkAJUhyD3VPLqZ8QxkAAmW86KA3T8D+SWahFhCmLiEhsf1IVUTwG2zQb8grFEpOJfghjI6UDnIRvdN5QbhSBaybHkDI+6RbOY+EJEJYGUmqOyQdQYjj1SAQM+JCA4oJvvpvvQb6sRYEWKe0pp1Ojbi6x6n0QUNkpouFm1/CE7GJ32inwnHgArs+Ejya35QleVqql35lywXxyYUQeKnuD64LSkI++VD0MDMqCV5Ft+FSaUyPrrJxKVBcIXbTo8TLAA+wJC7Q3yAGyEHJ5kpr4+TxT+TpKRjljOaH9iCe8FFqQ25npCnA2es01E9EQ1ZaFqbdfy8Cn0j8Bw1JJ7K1cWNmLQAFtB6vdD0a3k5NnqznhcLLJEajtJFk8bh+zZe1gm4gCNB0V22uKwrgzFyIgCYRi+bFz1ff4gZJUXstvKELT3506a+q9KNJ8SKqfi4B550Ep8FyigYkj0qwIY+wFjVI23QxBuAIQ10TM8IFoP/UMGw5QgaBg2ByPvBXmr7SCAkYsgaZb+4qbwRDIr+q4gyxepm+SH5wxnLoxKi/AkI4UCfRPxOEVFSV+gFDaTvIUyz6kU6NCGW2iZmaLKoLk8XuaEKEFtyoFlKIQUmxBi4GWQr4R+iClva4hHCx4aSta2gA7a3tpTz6ENzOWR0pQBvhzzQCnoAswtRgj5TgnzUw1tQYiSAEL6mAl8mHP+RNeUM+4GrfbiBNcJLoIELi2TR+NSBuCfV53mBIekkJp9Cld3CnS/m1erb0abBAoRboCJlUQGkEvZxSGzI/0CsuECeJ3NfMFXeHpExALBRNaTZ2B6s1YTcluNd20JVXNWLE5XKhPA89ABk7yUgQb/EUtzINbg2GI3F0V2OLidJelBXlQtZWw6j13gY9gwRhw7hTwahUIrymMRa4BwwOTdkCLhpjBk2SXSqBTteWoss3fbSj9HJK3PMGueDHi2TkqVuTjElgdUKPgvb5QFRvkBcxNz3ANm6bRhjA5g+6oa5QBVTUASsMsiKcrImGj1uTToSQhEz7IAPUg7qoyS2NwJABepDSIIYscNMhKDAkHAUP5sAN543KPJUUr/RPoWkAHrwcZIE+dTQFMhM8z5nMplUlnk6kovF2mIyKVuy4KzocNaw5jaZigQbDO7H+A9ljuq+bcTyyweYhaYyi13wAYKYm+Wsq1+ALBAXZg2YIoPRADSHNfZGWG5hY6OwWj4YxOvKhIe48qG7pjvE8Pm0fDQGA+wMGyQFaIU9ArJ3L+7AV3FGc41rBegDm6Gu8mmaLWFY2PmkrmnWVlUYGgjUpO4gvqiNWpBwmkQXyPlRBpBFcS0cZ5KUs/SLKKf2hUTJ3xR2dlpH4CDVkO6aCV68hk4GEAF42DBCIRYBpO71OTSGraAdgHCs36aoAH+qAEL7NR05QpaaBpUZppI1yHaiCZSgyDaa6I2RQX8evQyQTf8HV5KAAhasDCP6ecYOBdATaea1MLGKXqsazmYi4kNPs5Ohl/E/q3zh7KlKsDKCSTILLCMkqDZ6Esoh3a+nTchF7hhLiTrhsig5pdai5tULJaIIN5RFJYg2cU+JvtKtzR2oImEYMJZYKklRMc88KYwukHzryfceDW4jX1OsTHXZ/q9XwOlyXspLjPWlAMGzMMn4Yc7xGQQYJ71kHCgFLi5PgUkMzxCo5jTm0E+XBXyuR2flkNEwQPyKs6jgD8JZ1Cxj72Qw/UmVgUSdxFXJVZRawDVfa2Zq6YG6pl90zxUpQJ5d3OtHXgAmNwRrledVsWGDgGg2MagWRquwKIpgQbuoZba9TiJJhVYj8oYCDKB6jA4H1TeIqHTEROl0HRlw0U96xIov7G/YctAm2DVxRAbOqdmiWudwTC76Lkv7xhd+vY0ujvY7mWW75FCZKzGGmWlabXI2I9vqsDrmMysNYYtT+9tl8UH4gG+avXQfkhY+u0ewNJLOHT0hkXoeoyzrS8tBZxteg6lfLiAdRYKVW/Zwz6ZTAHHaSWnhTloSwAiJZZ5r0kc6dAleJSGu8zeKtOnzAPx3fYS+jNJtpzrezUdkDr6+xI0hYn4J+U/r5ECpjaFD3OjjjOrRA6Wj9zhIwTsEgSI2cq8Fj+H46PY5AKuM4ghSEpixVve8I+45iLwmQLEVUicDS3Ey2wrQC2AnS6i5h4MkNMRj0WaY2YHIMAoYkxg0sczecZeO1nnzRf7noaAG/J2Ei7tohXy/u76cMHach1LOktaeFgRk0wLA3E9K51/NrnylGIgIIJvRMh3wJwgQOXPf7He1UXqZJMIQyklKMpjlcKj/7PVCwOoCg/8HZrjEAX4jojY1YNPtw8H8CNdFiCBTVAYDqIR+DJXBmJnUaacKLpKIWDqoa1qT8iYFFiJuGJ3nHTdOp/NDbPCqrhg2P4R9gomM6JCWPODcvD7pyQNkFJOWUTNigBzLzVHWk0wwSXZcQDXBMf5aQEJXPOOCy4y4YhPmTZsJ50icbU4KhxhURHagSHg6ujKUza6LJGoDjl1lckWxpJexNoy+RAIJiIf6qcpjEWZ9B0UAxrVYSMYIGNLF54yI8Jemm+ugrLzyE+XN6FSht8vCqJLqEd5hmRYSWC+DMnN1yYwhdZ7UxmUbZ4NlZN0u/BUoYMaekQZcEUrseMtsyHpRaKiQdnTodIe48iG5fFA7KG53KMpZxmT7h/bSoDrAFOwXXaKhbYrq4dbZdVXQzB4ynQ5Jg0OiKju/TgVKhf6i5pYlN7UTq4kVQ4vSBaiR3GRN8K+BrXBWHp5ERMUoWpHAjHSwHgCSb0ByhiSpcalGfakK7gO2ZXYFfig31wB8dXWme6O/gApcrydOpI8M9/C+Osl890cG8+JcK2r+pIBXQJth9V50W6NBsqcGhz0QLgVzEBysmqbpn1vxTMLAN3IuNQlP4EF9ho09DcHlPr49uJfCFKGkVGLg1aFG8FOHORVdEhcF3UweLpDMghUgBsEV6Z81IdGxWpAtLHzrNfNofpUwgj853aEgkz9YnDbAVQUy159g6iF8TYYuA+lhVv9CQiDRIAktJFf/81E/PhGiCRrZ/ZtVBqfxV0kwMt6Lz63rcbx7FlKMgwpJshakxy/+W1R7T/jesNr5ZTVr8UAXkj/JHkDt8aNKS4tXHoppPGojRdYnUI2fozdnP1hCG0uQSxAehGnEfM00R2rgCzOAABk+5c1nIYOCePfsbmi6iu6xQ2SgKQ7hGdrlBwIIEaooCvaKPhGQNImiRSxqQlDraMlABYUmFA4bYCmQM4Z+WyBkvTgPxjlul5VkjmAELzUT7hIMlcOIzwkwX0kkeARzRjWvXjZ9ASWaoYt5DKVzNWIFLrDjWJusTCTdFEKa9YwxH/OhoQzzJtyk98CiBpOV0EHxxYEi1ZvrgRRT2wjdoAIqoygUaeaGh2Fq0Ut6HagZ9qUk32mjq81PRa+LDpQc1rHQAGukiD3XKhKIGsaQhnz8AV9yJb8aLUdUgvSwQmb6P0oH+8AapWcVdEA1oLvWElClMUYHQjeoImv461ClRIA1028HVSClSorSURhYqVVhf4wsMlrRYpsQWMuUJnKQDgQ5opuzuQrcSoh6fsDyaQqZ3+i/hQDWQNlqxr75mPmFvneRivHRyD8KhgqeO5Kur7wMVRYdtOq/T2UKHld4n4TrUMjWiLpKSpAqQ4T374HiJMU7FRnkHOwOXXRrEifPQ9DWV95GDzuarTuYS6dMMIS9cDj/DpahnJ1CeqKN2NNuht3CRrn8gpsku1ZYwKwcjCE/KxARsFEBPc5eJFkpInahBvWBOBxpzdYmBRuNj15VVfZDkvYMu3IgjAXqQrdCkBjzVkUW8c2tsiCR/4SRQO3kZa9IZNEBgEZkYIf/8G78PBAMpAv5q1yEhQF1AT2AJwZj1KbwWNaaS9pjErXo8jqsd7TYWdQo9XlhCwLGxkgJI+CLBmO9TC4LrCE8i3TKWRvaXnkMGkfBVsxtrFvT+1aerEoyvW7C8LMk35GH18Y8+jiZ+Bra7/wNoXPt9LUJBMQAAAYRpQ0NQSUNDIHByb2ZpbGUAAHicfZE9SMNAHMVf04oiFQeLiChkqE4WRUUcpYpFsFDaCq06mFz6BU0akhQXR8G14ODHYtXBxVlXB1dBEPwAcXJ0UnSREv+XFFrEeHDcj3f3HnfvAKFeZqoZmABUzTKSsaiYya6Kna8IYBj9EDEuMVOPpxbT8Bxf9/Dx9S7Cs7zP/Tl6lJzJAJ9IPMd0wyLeIJ7ZtHTO+8QhVpQU4nPiMYMuSPzIddnlN84FhwWeGTLSyXniELFYaGO5jVnRUImnicOKqlG+kHFZ4bzFWS1XWfOe/IXBnLaS4jrNIcSwhDgS1JGMKkoow0KEVo0UE0naj3r4Bx1/glwyuUpg5FhABSokxw/+B7+7NfNTk25SMAp0vNj2xwjQuQs0arb9fWzbjRPA/wxcaS1/pQ7MfpJea2nhI6B3G7i4bmnyHnC5Aww86ZIhOZKfppDPA+9n9E1ZoO8W6F5ze2vu4/QBSFNXyzfAwSEwWqDsdY93d7X39u+ZZn8/2QRy0FK0Qb4AAAAJcEhZcwAALiMAAC4jAXilP3YAAAAHdElNRQfkBB0WJSo9ERxFAAAABmJLR0QAAAAAAAD5Q7t/AAATOElEQVR42u2dCXQVRRaGXzYSIAohEBATBEQYQfSIIwybCDrKCIgso+OCC4OO6KioiKJiRFBWh3EdPYorIoMoioOIIiGRRQggKHsGJBsJCSBZCUiSuTfcZtq2u7q6u7rfe5165/yH8JKurq6ur5Zbt24FamtrA6GgazJ3RIFGgb4BFYD2gt4Edc4pqwiESj5FqKCgIACf3qDVoCJQsUqfrVixwlfPKxV8hQrkrUGrQbU6OgF6bMjGnRG+KfRAoB2oHFSroywJupTvQAeI24L2GUCu1stDMndE+AT0Rw0gl6BL+Q90gPcc0E8ckPsKdvhMl6BL1QvQCfJ9FiBX9FK4wy5Bl6oXoDuAXNGL4Txnl6BL+R50gLSNQ8jDHnYJupSvQSfI9wqAXNEL4Qi7BF3Kt6ADlCmCIQ9b2CXoUr4E3YWeXKt/hhPsEnQp34GefqAYQV/oIuSnYb82TGCXoEv5tUdPAGV6APuccIBdgi7l5zm6V7D/I9Rh9wvo2dnZAcxrQkJCRHR0dFSDBg0i8Wf8Dn/ndX6ysrJO56d169Z1PwezLJX7x8bGRmL5xMXFRYrIE3yagvpq1IO+/7267CMjI2Pqfu8W1HqwEewb6zvsboMeERHRENL5HUvwN03sgA3XNYbr/wR6BjfggH4E4S6dEtDPoHzQJtCHoAmgbtgAuNXQQH7i4R5DQTNAy0A7KT+Yj92gNNDroLtBHd0EH9OmfQyjQa+CVoH2gg6BSmnT0l76/hXQraCW69evt1p/OoDmgN6iZ8WfJ4OuBP0Caq362+vq6pULkCeDskCzRmzeHZCwewf6kSNHAgRyFiN9RaN4042Pj4+Av78K9BGokiPt3zwT6GGAspEowOFzAehtUIXFvPwAegAbCBF5KSkpUXrYB0Hfg2os5gfBXArqb3UUBJ/2oO+U+kKgH8C9FPj/rVu34nef1n3nEuQKbLOHG8O+yQPYnw9F2N0AnSr/1dSrsipWFULOU6kozT9SD10rQDmggU6G9TRaeRF00mFeDmIvj8NqB3lpBGk8BToqqHw+ASU5BP0lUCZOX7BnJ9AzRUJ+tgZyRTMZw/h6Cbto0JOSkvCl3s9R+QtBPS3MA+cJqsBqYY83wQ7s8GkB2ig4PziMPttGo4rz4n0ulA82hl1OnjxpF/TpNFXpBZoIGlZXZgIh38OAbYYB7M08gn12KMEuEnQytrzGUYFwWNmGM38dQXtcqMRq3Wex94zFSu1SXnCee6kFyP9OQ+5aF/PTxgHo3UFvgtKh3GKEgM4B+emePbe8MhBM2Idt2uUr0OGTSMYm0yEh75wU/i6BhrW1LgtBuYQnT/n5aFcLTHE5Py9xlvlfPSgb1GqzaYUR6GvXrg3QyGdqUVFRwDHoFiA/3bMbGOgQ9s0ewD5r+OZdvgDdgtFtKi7rWOg5G8A133D2OmhZvgGHmjSsbgm6GHQHaCHohFllXrduHU+eEjmMbmgkXAC6BzQINBh0J+gFssSzrsXnjed8dz3Igm5WPkjbE6ArQG1BzUDngAaAHgdtdWowJRtBT2UaRA1/R/r5fPw//dzTafinPTZgm84YxnsB+0w9A2G4gE5Dx4EcBqBjoJttzoUbMWDHCvrnqKioaFYaFRXIZV3FXmqSzx4c+bnXJA00OLWqqqpilVlP0Fc6136NBj6L5dPbAHa0kcxFyMzKnewqw01GT1tELQc6gXy3A9imDdcZRsP3id7BvivsQO/QAZdPAw9wGN2wt+2Rk5PjJI8I+0pVmmXYW0ZHR1uyUsfExETCde8z8jqLY078GeP6DGh0onjykpycjGndpRppfAFqaLVsaCqhhT0TdKGNJbJzGbDXKD2056ALgPx0z55dWm4E+/cewD4jWD27HdDJ6PY6x3BvMyhFUD4V2NG63NnushgOiynarV5+17F6LXJC2c943gE2LOYDyWEl1m7Z5ObmYjp9qAF8A6c8DnwfRjGeb7TnoAMcZ4F2CYRtmsGc3TPYRwQBdqugWzG6gRqLzCtZu5s4SQOH1OTFpZfnIg7Qyxg9XtNgjcwI0hZNmjSJcFjG8Qwr/hRPQXcBckXPGczZvYJ9utewWwGdjCpZHGvTzzlx/vDgmWcb5L0UhvdRJqCz7BFtQvWZLbrOGhkb53gGuouQK3rWoGdvDtriN9h5QLdgdMPu8pZgbCLxEHSWhXpqqD97WICeXlC3n3yRB7A9m6tzIouHsE8b6RHsZqDPmIH7MwLjeD3dwqGi2wWdrn2FUQbV6GeOu8Mk6M579BagHzyBXd+pBmHf6gXs+ZVVwQYdNyW8wTEfx11jbUtLSz2rkOQ/jVOJ23GqQJtKFpHeph1kY9FIhbvc1A2QQ9B7c5THDtpYck4wGj7aSYfGy/6UD/THn092kwXkvYh+8X9BHwiYZkWEHOgewz5lmP7Sm1ewP6fX2HgIOo9w++WZXlVg8sCaSX7YVjzf0GlkEqirE9ApD8st7pabSw4znbG3dwt+3IJLDjqLyXeBN4+HyKnoNjR2BgV0qOxn5On3rAj7j17Anmt8/7CHXQDoqGfj4uIiXG6QmlFPJMKvu8Iu6JSXtgSHnXvjdR+D/gY6SwT01PhcRsuZTsumkrG91R3QoZK3Au0ATTaYMyd5Brv+/T2bRuS5BLsg0FHv4fq6S704DpfzPPDp5gK9shJZCFwKOizAxx4DVFydkZFhq3zQaQiun0b2AbfLRzzo5Ia6XVXZgw+7cc/uBexP5ZRWhDLoqKWiAjqoIL+GrPlmwKAP+Qoati6njRSH3QAdRRs0cBrxraCy+xrn9BYhj4JrPuBIG+MCbCAX4CXkeITldTyooEOljgGl61T2p3OMYd/mAWyT9dxVPYR9VHlwQeeJWJImas5Om1JYG0i+xNBNWmObxrCUTHPPrzjyzw26JuLNCAKpxiHsOKz/A0aK4WwEZzPSKqF3e2HDhg0jtOWD/ycPRxyZPM1p8xAHem5ZJYIzk1HZU/V6Ni9hzwneyKIS1CVIoGPPcR4FRzD72zVOYaeda9sZlfhaK/Nb1Tw2VyTomkYFQ0ql0vMftwk7jkLac7y3KxgNCza2rWx4HU4xaazEgQ4VuTvopEmFTzUYxrf0CvYgTiPWDcrcEekh6PjiUxMTE+uMbbhTDP7/PGe0lEYO8nWPQbq4nt8PN3PYTLcTI9acbdC10JMraX+KrLKYliq5h/FpaWmG6WMPTcE79K7NtLM5RtUYTnUddDzhBCpyGmeFT8Xe3wD27R7A/rTBnN0L2G/wCHSc+96uN+yjoAdme7yX2THQUe9o1JvPdWKpprzPdxN0vXvS0te5FBGGxzrei/HO+jjZasuxunHSVdChAnezaqBi9OxewJ5qEHDSbdjXX7dpZ9D3o1Oo5XKzIb+VYBOqaCVG6V0u4Jlf9RJ0g/3fo00ayjmM/E8zuGaP0z3jNGKrchv0mTYq/aScIMMepGlEu2CDrtoLXWIC+3QrvTAFQTCaRiQJeOa5wQRdNbKYyCizDXplT9ctM7jmYwE7BBsyGiBhoK+zWeknGQzjlXV4P8J+awiFkurJAfsYXtgp/JMR6C0FALYq2KDTc6YwyqtY7ZqqmdZsMLhmsYA8dXJ9jg6Vt9BBxQ827E8ZzNkxll2OG/cLFdAJnn4mbpdohe7DmSdW8IOBDp83kWER9xT0yMjIOIaVu1RvcwyBnmFwzX4BRyyN9wL0CoeV/0mDntUr2CeN1J+zXwo6LjoEVShFgcUNLRS3+6TJJpnWHHliGZvm2TXGUYM0S8TyGqXVDX3nHZR9R9aautGefvi8y7juMgf5OdPEAzFkQEc9EWzYtffHMFXw/UTRUWRDLa47Vf5xJkP4DOjJojminFQxltd62YR8sElDxAU6pXUFLdPhlOVKq40PpfEqIy8bjcqeLPeGc3soX8uhpMjLbpEnDjOCQCfYK42CVuz0APZxWtjJ22+3n0FHtW/fnmXsOr0JhgUGQbDEZGTA3ZOqAjFWOfWMy8vLU46bqtA0Puil1qSwsNA0P40bN0ar+6MmzimzGO8s2WRzz3wrcePg0xz0uWeecQJBDzbs6PBzuc69b/M76JRmnMlxRdVmQ0zqMVmVDpf1JqD7K2OpKIp85VeL8HWnQwxZvvdH6byx/ujzr5SdcqQzxZrHU0vXczgpXWTSEC7gCMw5oLq6mpVGc2pwDnnq6y4YdNTjBsN4t8NRofaBGmruGwcq8TvoquN0WZZ4jOZ6hkll/pSj8pVRQIVJdBzxWAqqsJAR7dUW6LiGzzEqUPfyGDF2Czn/FFvIh6kdgnwNeE5vxeOs/gV6iEY195NnY4aNLb8hCzpqYhBhH/urynvwEN53cX0AndbYbzM7eohVoXHNHJTtwnbL/9oEvTfnyShOtFc51YSlAwcO8JSvHe33Yh29wiXgJhoM41u7DHuazj0n1wfQVYc8fG4yhO/OcdxTvsCK/I5dzziKn96FzjWvdQmy8ywa9CYIvP8JmjJVhSvowYJ9f1p+kfZ+94UY6A+6BbrKKaSMdS5YSkqK4fV4ZC+GUeaY1/JoPp3oOdmJ1Z12eqUyNsfY0XI7zkAE+80mZcwbdWcobbstCGfQUY8aDOMR9p9cuF+2DuhjQwz05oy1U8egU0V8wqSSDeL0wR5nY95dS9eMQR9zVeNz2Mk6ek0N2ssCZ9F896gDwDCQ5PUtWrSIcPgeMbzVv21GmkFPwfNVaT0c7qCzYO8CKqtvoKuWazCMcToZaRTNWLNmjYj04ylOWoaBxljwxW5MG0KWmfRilXQ44916p5OSq+f7mnzMx+iyVk9IoYirGFF1HjWaNSZGOgyo+TIa93DLqWD/+Y4UFXebic9APp1Y00/7jtu1axeg1YHlmvK5MZxAJ9h/PYw/UlMH4a31EfRwFEVIaUBz+IF0ZPL19HNnHF7j2dxe54mGvi3Ju28kufPeAhqCnnSQr6aiTiXl2BOfQEcrDwXdRJ6LGHzjbPSj51nzd8XH30PQUQ9re/a8imOYh+USdCkp/4BeDbpOZwh/MahGgi4l5Q/QUUdwPf1XSyjQy1uIciNBl5IKA9BRr2eXlGlhvFeCLiXlL9AxsmpjDYzdJOhSUv4CHdVL5zx0CbqUlM9A76+BMVaCLiUlQZegS/laUL+uBY0HXSJBl6BL+Rf0DVTPpknQJehSEnQJeriAnv1ziRKtdgLoA9ACfMGgfoMzd7hy7vmwTbvwnueDRmNQT9KNGCIb3ZMLq07g7xuBrkEvRjwZB/QAqA/I1UiteOQVBfS8G4OXYGgw0CBQfG1NjWewDd64A08u6goag7EI8V+9fRpCdOI4lve5oL4kJb7iPNV3zSXoYQp6dvFhzNPNjLJH56FkURWq+ES1snz5ncH9ToDeJriOGPwNhgLr5eRoJt395qcO+BzMCCL6M8LvGmxK0I5Trti9QN9r7n8wveCQK/dcdaAY77nWpM7NlqCHKeiQTkdVKGo8+vkR0IOgz8hNGL/fk15QLGpI2EoVTquGhojvENyZOs9ZCvoP6A3QF6BjKv+Hi0RV9JxTkXof0bg/7wWt1AH/epeHze3p+ZQyWkflM96tRmZlbiHedyGomPQL3b9C9d0jEvTwBT1VCZKhdhzKOeUKjMO1A6A79I6qtnm/q1TPMFB9nBbdc7Tq98tAp49frikvUyBQDvr4QNj0paQO9IcoXTw3r/fKH7ar89VVdcTXppU/bncT9OEqyPuKKntT2LP2B1ZBg47C8/2UsxKU73IENTIS9OCA/hql90nBseO689Ucgb2ICnQcojcwmB8fpr8ZbZDGdPp9ptihex3QI0FnGNx3iJJ3kUdXM0CvEDWScmCMe1Ia4/wB+v2U3lHQBW7PP81AxzkofJ9vAvpjSs/qRh5prt4U1J1GNZ0wr6ABqp2PsRJ0CXo4gd5EFUbrF5qbY/z5RO1mn/oAOsG9nGLza20FGyToYkA/KkHn1lSBLxVDX3+oMsAo0GNo6guyiw75HnScB0N6t2iMUFjZvwVlqQyTEnQBoO+XoHNrggvz0yTQXWTdPq6ybvf1O+i0hlylLCPhmrkyjUk/ZZxKJuu/BF0A6Msl6Nwa6tZLpjlqimqte9OaoiN+B12Jt78NHVUM/qanBF0M6OMl6FzC3jbBg5c9UHWWXKzPQX+L0vzayCCpArA+gK408pPcAL25wLPJ/Az6myKWvMr/H2Rjpl7FJZfUWhrSxvgc9MdUz9pJ5/cJoC31CPQvKA8vlosGHV0ayR2zWoJuKDQMJQp6mficBZTuVtAIUDNaWkJX0Fz63RJRy24hDDr6+pdTuoXkgtuDfOtxD0BOPTPGPUN5OESOROj331UI6Cqj0FBQngT9N1qCLqTCLM2nyvoelRFKqcTqhhbfQ3uPjXF5XoOefbRMqXfHWEds1SPQU1SOS4rmCANddaMGtPl9FmgR6EsXdZHm3jGC0n1vZd5B7XMNsnD9UtC71KN03lZeKfyFEuzn4RANfdoJwCraOILW5yTBFQhHDB+RX3WUgfV/PD1/b4M00B31c9FbKGmJDX3/51JjU0PLbLjZ406aWi6k+uimZxzuB/gE9P7K3fuCuVW1A+gFGsbj+7hJRLr/Awjyex8Qbg9AAAAAAElFTkSuQmCC"

local function GetIPos(ix, ctl_group)
  if ctl_group == "Smart Plugs" then
    local rowlen = 5
    local base={ x = 10, y = 80 }
    local ofs={ x = 68, y = 110 }
    local row,col = (ix-1)//(rowlen),(ix-1)%rowlen
    x = base.x + col*ofs.x
    y = base.y + row*ofs.y
  elseif ctl_group == "Smart Power Strips" then
    local rowlen = 3
    local base={ x = 140, y = 80 }
    local ofs={ x = 68, y = 130 }
    local row,col = (ix-1)//(rowlen),(ix-1)%rowlen
    x = base.x + col*ofs.x
    y = base.y + row*ofs.y
  elseif ctl_group == "Smart Bulbs" then
    local rowlen = 2
    local base={ x = 10, y = 80 }
    local ofs={ x = 190, y = 130 }
    local row,col = (ix-1)//(rowlen),(ix-1)%rowlen
    x = base.x + col*ofs.x
    y = base.y + row*ofs.y
  else
    local rowlen = 4
    local base={ x = 24, y = 80 }
    local ofs={ x = 50, y = 50 }
    local row,col = (ix-1)//(rowlen),(ix-1)%rowlen
    x = base.x + col*ofs.x 
    y = base.y + row*ofs.y
  end
    return x, y
end

function setLogo(tbl)
  table.insert(graphics,{
    PrettyName = "Logo",
    Type="Image", Image = KasaLogo,
    Position={logo_pos.x, logo_pos.y}, Size={125,38}, ZOrder=10000
  })

  table.insert(graphics,{
    Type="GroupBox",
    Fill={255,255,255,255},
    Position={0,0}, Size={page_width,48}
  })
end

--CONTROLS
if CurrentPage == "Settings" then

  page_width = 360
  page_height = 176

  logo_pos = {x = (page_width/2) - (125/2), y = 5}
  setLogo(logo_pos)

  status_pos = {x = 0, y = 50}

  table.insert(graphics,{
    Name = "Status", 
    Type = "GroupBox", Text = "Status",
    CornerRadius = 8, StrokeWidth = 1,  StrokeColor = {0,0,0,255},  
    HTextAlign = "Left",
    Position = {status_pos.x, status_pos.y},  Size = {page_width, 60}
  })

  layout["Status LED"] = {
    PrettyName = "Status~Status LED", 
    Style = "Indicator", IndicatorType = "LED", 
    Position = {status_pos.x + 8, status_pos.y + 30}, Size = {16,16}
  }
  
  layout["Status"] = {
    PrettyName="Status~Status", 
    Style = "Indicator",  TextBoxStyle = "Normal",  
    Position = {26,72},    Size = {320,32}
  }

  login_pos = {x = 0, y = 114}
  
  table.insert(graphics,{
    Type = "GroupBox", Text = "Cloud Login",
    CornerRadius = 8, StrokeWidth = 1,  StrokeColor = {0,0,0,255},
    HTextAlign = "Left",
    Position = {login_pos.x, login_pos.y}, Size = {page_width,116}
  })
  
  layout["Code"] = {
    Style = "Text", TextBoxStyle = "Normal", 
    Position = {0,0}, Size = {0,0}
  }

  layout["Username"] = {
    PrettyName="Status~Username", 
    Style = "Text", TextBoxStyle = "Normal", 
    Position = {140, 166}, Size = {200,16}
  }

  layout["Password"] = {
    PrettyName="Status~Password", 
    Style = "Text", TextBoxStyle = "Normal", 
    Position = {140,184}, Size = {200,16}
  }

  layout["Server"] = {
    PrettyName="Status~Server", 
    Style = "ComboBox", TextBoxStyle = "Normal", 
    Position = {140,148},   Size = {200,16}
  }

  layout["Connect"] = {
    PrettyName="Status~Connect", 
    Style = "Button", ButtonStyle = "Toggle",
    Position = {300,202},  Size = {40,16}
  }

  --LABELS
  table.insert(graphics,{
    Type = "Label", Text = "Server",
    HTextAlign = "Right",
    Position = {10,148},   Size = {128,16}})
  
  table.insert(graphics,{
    Type = "Label", Text = "Username",
    HTextAlign = "Right",
    Position = {10,166},   Size = {128,16}
  })
  
  table.insert(graphics,{
    Type = "Label", Text = "Password",
    HTextAlign = "Right",
    Position = {10,184},   Size = {128,16}
  })

  table.insert(graphics,{
    Type = "Label", Text = "Connect",
    HTextAlign = "Right",
    Position = {170,202},  Size = {128,16}
  })

elseif CurrentPage == "Smart Plugs" then
  page_width = 360
  page_height = 176

  logo_pos = {x = (page_width/2) - (125/2), y = 5}
  setLogo(logo_pos)

  for i=1, props["Smart Plugs"].Value do
    x, y = GetIPos(i, "Smart Plugs")
    table.insert(graphics,{
      Type = "GroupBox",Text = "Plug "..i,
      CornerRadius = 8, StrokeWidth = 1, StrokeColor = {0,0,0,255},
      HTextAlign = "Left",
      Position = {x,y},Size = {64, 100},
    })

    layout["Smart Plug "..i]={
      PrettyName="Smart Plugs~Smart Plug "..i,
      Style="Button", ButtonVisualStyle = "Flat", Padding = 5, StrokeWidth = 1, 
      FontSize=9, VTextAlign = "Center", WordWrap = true,
      UnlinkOffColor = true, Color = {255,255,255,255}, OffColor = {200,200,200,255},
      Position={x+8, y+40}, Size={48,48}
    }

    layout["Smart Plug RSSI "..i]={
      PrettyName="Smart Plugs~RSSI "..i,
      Style="Button", ButtonVisualStyle = "Flat", Padding = 0, StrokeWidth = 0, 
      FontSize=9, VTextAlign = "Center", WordWrap = true,
      UnlinkOffColor = true, Color = {255,255,255,100}, OffColor = {255,255,255,100},
      Position={x + 38, y + 24}, Size={16,16}
    }
    
    page_height = y + 60
  end
  table.insert(graphics,{
    Type = "GroupBox",Text = "Devices",
    CornerRadius = 8, StrokeWidth = 1, StrokeColor = {0,0,0,255},
    HTextAlign = "Left",
    Position = {0,50},Size = {page_width, page_height}
  })

elseif CurrentPage == "Smart Power Strips (KP303)" then

  page_width = 360
  page_height = 176

  logo_pos = {x = (page_width/2) - (125/2), y = 5}
  setLogo(logo_pos)
 
  for i = 1, props["Smart Power Strips (KP303)"].Value do

    ps_offset = 130
    page_height = ps_offset*i

    table.insert(graphics,{
      Type = "GroupBox", Text = "Power Strip "..i ,
      CornerRadius = 8, StrokeWidth = 1, StrokeColor = {0,0,0,255},
      HTextAlign = "Left",
      Position = {10, ps_offset*(i-1) + 70}, Size = {340, 120},
    })
    
    ps_label_pos = {x = 20, y = ps_offset*i}

    layout["Smart Power Strip "..i] = {
      PrettyName="Smart Power Strips (KP303)~Power Strip "..i.."~Device Name",
      Style="Text", ButtonVisualStyle = "Flat", Padding = 2, StrokeWidth = 0,
      FontSize=9, VTextAlign = "Center", HTextAlign = "Left", WordWrap = true,
      UnlinkOffColor = true, Color = {200,200,200,255}, OffColor = {200,200,200,255},
      Position={ps_label_pos.x, ps_label_pos.y - 20}, Size={100,24}
    }

      last_plug = i*3; first_plug = last_plug-2

      for plug_count = first_plug, last_plug do
        x, y = GetIPos(plug_count,"Smart Power Strips")
        table.insert(graphics,{
          Type = "GroupBox", Text = "Plug "..plug_count,
          CornerRadius = 8, StrokeWidth = 1, StrokeColor = {0,0,0,255},
          HTextAlign = "Left",
          Position = {x,y}, Size = {64, 100},
        })

        layout["Smart Power Strip Plug "..plug_count] = {
          PrettyName="Smart Power Strips (KP303)~Power Strip "..i.."~Smart Plug "..plug_count,
          Style="Button", StrokeWidth = 1, ButtonVisualStyle = "Flat", Padding = 5,
          FontSize=9, VTextAlign = "Center", WordWrap = true,
          UnlinkOffColor = true, Color = {255,255,255,255}, OffColor = {200,200,200,255},
          Position={x + 8, y + 40}, Size={48,48}
          }

        layout["Smart Power Strip RSSI "..plug_count] = {
          PrettyName="Smart Power Strips (KP303)~Power Strip"..i.."~RSSI "..i,
          Style="Button", StrokeWidth = 0, ButtonVisualStyle = "Flat", Padding = 0,
          FontSize=9, VTextAlign = "Center", WordWrap = true,
          UnlinkOffColor = true, Color = {255,255,255,100}, OffColor = {255,255,255,100}, 
          Position={x + 38, y + 25}, Size={16,16}
        }
      end
  end

  table.insert(graphics,{
    Type = "GroupBox", Text = "Devices",
    CornerRadius = 8, StrokeWidth = 1, StrokeColor = {0,0,0,255},
    HTextAlign = "Left",
    Position = {0,50}, Size = {page_width, page_height + 20}
  })

elseif CurrentPage == "Smart Bulbs" then
  
  page_width = 390

  logo_pos = {x = (page_width/2) - (125/2), y = 5}
  setLogo(logo_pos)

  for i=1, props["Smart Bulbs"].Value do
    x, y = GetIPos(i, "Smart Bulbs")
    table.insert(graphics,{
      Type = "GroupBox",Text = "Bulb "..i,
      CornerRadius = 8, StrokeWidth = 1, StrokeColor = {0,0,0,255},
      HTextAlign = "Left",
      Position = {x,y},Size = {180, 116},
    })

    layout["Smart Bulb "..i]={
      PrettyName="Smart Bulbs~Smart Bulb "..i.."~On/Off",
      Style="Button", ButtonVisualStyle = "Flat", Padding = 5, StrokeWidth = 1, 
      FontSize=9, VTextAlign = "Center", WordWrap = true,
      UnlinkOffColor = true, Color = {255,255,255,255}, OffColor = {200,200,200,255},
      Position={x+8, y+40}, Size={48,48}
    }

    layout["Smart Bulb Color "..i]={
      PrettyName="Smart Bulbs~Smart Bulb "..i.."~Color",
      Style = "Text", TextBoxStyle = "Normal", 
      Position={x+100, y+10}, Size={72,16}
    }

    layout["Smart Bulb Hue "..i]={
      PrettyName="Smart Bulbs~Smart Bulb "..i.."~Hue",
      Style = "Text", TextBoxStyle = "MeterBackground",
      Position={x+100, y+28}, Size={72,16}
    }

    layout["Smart Bulb Saturation "..i]={
      PrettyName="Smart Bulbs~Smart Bulb "..i.."~Sat",
      Style = "Text", TextBoxStyle = "MeterBackground",
      Position={x+100, y+46}, Size={72,16}
    }

    layout["Smart Bulb Value "..i]={
      PrettyName="Smart Bulbs~Smart Bulb "..i.."~Val",
      Style = "Text", TextBoxStyle = "MeterBackground",
      Position={x+100, y+64}, Size={72,16}
    }
    
    layout["Smart Bulb Kelvin "..i]={
      PrettyName="Smart Bulbs~Smart Bulb "..i.."~Kelvin %",
      Style = "Text", TextBoxStyle = "MeterBackground",
      TextColor = {0,0,0,0},
      Position={x+100, y+90}, Size={72,16}
    }

    layout["Smart Bulb Kelvin K "..i]={
      PrettyName="Smart Bulbs~Smart Bulb "..i.."~Kelvin K",
      Style = "Text", TextBoxStyle = "NoBackground",
      StrokeWidth = 0, Fill = {0,0,0,0},
      Position={x+122, y+93}, Size={27,10}
    }

    table.insert(graphics,{
      Type = "Label", Text = "Color",
      HTextAlign = "Right",
      Position = {x+50, y+10},   Size = {48,16}
    })

    table.insert(graphics,{
      Type = "Label", Text = "Hue",
      HTextAlign = "Right",
      Position = {x+50, y+28},   Size = {48,16}
    })

    table.insert(graphics,{
      Type = "Label", Text = "Sat",
      HTextAlign = "Right",
      Position = {x+50, y+46},   Size = {48,16}
    })

    table.insert(graphics,{
      Type = "Label", Text = "Val",
      HTextAlign = "Right",
      Position = {x+50, y+64},   Size = {48,16}
    })

    table.insert(graphics,{
      Type = "Label", Text = "Kelvin",
      HTextAlign = "Right",
      Position = {x+50, y+90},   Size = {48,16}
    })


    layout["Smart Bulb RSSI "..i]={
      PrettyName="Smart Bulbs~RSSI "..i,
      Style="Button", ButtonVisualStyle = "Flat", Padding = 0, StrokeWidth = 0, 
      FontSize=9, VTextAlign = "Center", WordWrap = true,
      UnlinkOffColor = true, Color = {255,255,255,100}, OffColor = {255,255,255,100},
      Position={x + 38, y + 24}, Size={16,16}
    }
    
    page_height = y + 60
  end
  
  table.insert(graphics,{
    Type = "GroupBox", Text = "Devices",
    CornerRadius = 8, StrokeWidth = 1, StrokeColor = {0,0,0,255},
    HTextAlign = "Left",
    Position = {0,50}, Size = {page_width, page_height + 20}
  })
end

