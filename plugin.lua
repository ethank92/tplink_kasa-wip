--[[ #include "info.lua" ]]

function GetColor(props)
  return { 74, 203, 214 }
end

function GetPrettyName(props)
  return "TPLink Kasa \n" .. PluginInfo.Version
end

local pagenames = {"Settings", "Smart Plugs", "Smart Power Strips (KP303)", "Smart Bulbs"}

function GetProperties()
  props = {}
  --[[ #include "properties.lua" ]]
  return props
end

function GetPages(props) --optional function if plugin has multiple pages
  pages = {}
  for ix, name in ipairs(pagenames) do
    if name == "Settings" then
      table.insert( pages, { name = pagenames[ix] })
    elseif props[name].Value > 0 then
      table.insert( pages, { name = pagenames[ix] })
    end
  end
  return pages
end

function GetControls(props)
  ctrls = {}
  --[[ #include "controls.lua" ]]
  return ctrls
end

function GetControlLayout(props)
  layout   = {}
  graphics = {}
  --[[ #include "layout.lua" ]]
  return layout, graphics
end

--Start event based logic
if Controls then   
  --[[ #include "runtime.lua" ]]
end